use super::builders::*;

use crate::collision::bvh::Bvh;
use crate::collision::systems::*;
use crate::collision::{Collider, CollisionShape};
use crate::physics::systems::*;
use crate::physics::{Body, Contact, DistanceJoint};
use crate::prelude::*;

///
pub struct World {
    bodies: Vec<Body>,
    colliders: Vec<Collider>,

    gravity_system: GravitySystem,
    movement_system: MovementSystem,
    shape_system: ShapeSystem,
    broad_phase: BroadPhaseSystem,
    narrow_phase: NarrowPhaseSystem,
    constraints: ConstraintSystem,
}

impl World {
    /// Returns the vector pointing up relative to the World
    pub const fn up() -> Vector2 {
        Vector2 { x: 0.0, y: 1.0 }
    }

    /// Returns the vector pointing down relative to the World
    pub const fn down() -> Vector2 {
        Vector2 { x: 0.0, y: -1.0 }
    }

    /// Returns the vector pointing right relative to the World
    pub const fn right() -> Vector2 {
        Vector2 { x: 1.0, y: 0.0 }
    }

    /// Returns the vector pointing left relative to the World
    pub const fn left() -> Vector2 {
        Vector2 { x: -1.0, y: 0.0 }
    }

    /// Creates a new World
    ///
    /// ## Arguments
    ///
    /// * `gravity` - The gravity force of the world
    pub fn new(gravity: Vector2) -> Self {
        Self {
            bodies: Vec::new(),
            colliders: Vec::new(),

            gravity_system: GravitySystem::new(gravity),
            movement_system: MovementSystem::new(),
            shape_system: ShapeSystem::new(),
            broad_phase: BroadPhaseSystem::new(),
            narrow_phase: NarrowPhaseSystem::new(),
            constraints: ConstraintSystem::new(),
        }
    }

    /// Returns the BVH used to accelerate collisions
    pub fn bvh(&self) -> &Bvh {
        &self.broad_phase.bvh
    }

    /// Returns a slice of all the Bodies in he world
    pub fn bodies(&self) -> &[Body] {
        &self.bodies
    }

    /// Returns a slice of all the DistanceJoints in he world
    pub fn joints(&self) -> &[DistanceJoint] {
        &self.constraints.joints
    }

    /// Returns a slice of all the Colliders in he world
    pub fn colliders(&self) -> &[Collider] {
        &self.colliders
    }

    /// Returns a slice of all the Contacts (collisions) in he world
    ///
    /// The slice only contains all contacts since the last time the Worlds
    /// `update()` was called. If Bodies have moved since then, it won't be
    /// reflected in the returned Contacts until `update()` is called again.
    pub fn contacts(&self) -> &[Contact] {
        &self.narrow_phase.contacts
    }

    /// Returns a slice of all the CollisionShapes corresponding to the Colliders
    ///
    /// The slice only contains all contacts since the last time the Worlds
    /// `update()` was called. If Bodies have moved since then, it won't be
    /// reflected in the returned CollisionShapes until `update()` is called
    /// again.
    pub fn collision_shapes(&self) -> &[CollisionShape] {
        &self.shape_system.shapes
    }

    /// Returns the Body with the given ID. Panics if no such body is found.
    ///
    /// ## Arguments
    /// * `id` - ID of the returned Body
    pub fn body(&self, id: usize) -> &Body {
        &self.bodies[id]
    }

    /// Creates a new Body and returns its builder object
    pub fn create_body(&mut self) -> BodyBuilder {
        self.bodies.push(Body::new(self.bodies.len()));
        let body = self.bodies.last_mut().unwrap();
        BodyBuilder::new(body, &mut self.colliders)
    }

    /// Returns the builder object for the given Body
    pub fn update_body(&mut self, id: usize) -> BodyBuilder {
        let body = &mut self.bodies[id];
        BodyBuilder::new(body, &mut self.colliders)
    }

    /// Creates a new DistanceJoint between two Bodies and returns the joints
    /// builder object
    ///
    /// ## Arguments
    ///
    /// * `body1` - The ID of the first Body to connect the DistanceJoint to
    /// * `body2` - The ID of the second Body to connect the DistanceJoint to
    pub fn create_distance_joint(&mut self, body1: usize, body2: usize) -> DistanceJointBuilder {
        let body1 = &self.bodies[body1];
        let body2 = &self.bodies[body2];

        let mut joint = DistanceJoint::new(self.constraints.joints.len());
        joint.body1 = body1.id;
        joint.body2 = body2.id;
        joint.distance = (body1.transform.position - body2.transform.position).length();

        self.constraints.joints.push(joint);
        let joint = self.constraints.joints.last_mut().unwrap();
        DistanceJointBuilder::new(joint)
    }

    /// Returns the builder object for the given DistanceJoint
    pub fn update_distance_joint(&mut self, id: usize) -> DistanceJointBuilder {
        let joint = &mut self.constraints.joints[id];
        DistanceJointBuilder::new(joint)
    }

    /// Runs a single simulation round for the objects in the World
    ///
    /// ## Arguments
    ///
    /// * `dt` - The timestep to use in the simulation (in seconds)
    pub fn update(&mut self, dt: Float) {
        self.gravity_system.update(dt, &mut self.bodies);

        let time = std::time::Instant::now();
        self.movement_system.update(dt, &mut self.bodies);
        println!("Move {} ms", time.elapsed().as_millis());

        let time = std::time::Instant::now();
        self.shape_system.update(&self.colliders, &self.bodies);
        println!("Shape {} ms", time.elapsed().as_millis());

        let time = std::time::Instant::now();
        self.broad_phase.update(&self.shape_system.shapes);
        println!("Broad phase {} ms", time.elapsed().as_millis());

        let time = std::time::Instant::now();
        self.narrow_phase
            .update(&self.shape_system.shapes, &self.broad_phase.contacts);
        println!("Narrow phase {} ms", time.elapsed().as_millis());

        let time = std::time::Instant::now();
        self.constraints
            .update(dt, &self.narrow_phase.contacts, &mut self.bodies);
        println!("Constraints {} ms", time.elapsed().as_millis());
    }
}
