use crate::physics::DistanceJoint;
use crate::prelude::*;

/// Helper for building new DistanceJoint structs
pub struct DistanceJointBuilder<'a> {
    joint: &'a mut DistanceJoint,
}

impl<'a> DistanceJointBuilder<'a> {
    pub(crate) fn new(joint: &'a mut DistanceJoint) -> DistanceJointBuilder<'a> {
        Self { joint }
    }

    /// Finishes building the Body and returns its unique ID
    pub fn finish(self) -> usize {
        self.joint.id
    }

    /// Sets the oscillation frequency for the distance joint
    ///
    /// ## Arguments
    ///
    /// * 'frequency' - The new frequency
    pub fn with_frequency(self, frequency: Float) -> Self {
        self.joint.frequency = frequency;
        self
    }

    /// Sets the oscillation damping for the distance joint
    ///
    /// ## Arguments
    ///
    /// * 'damping' - The new damping coefficient
    pub fn with_damping(self, damping: Float) -> Self {
        self.joint.damping = damping;
        self
    }

    /// Sets the distance the DistanceJoint attempts to keep between the bodies
    ///
    /// ## Arguments
    ///
    /// * 'distance' - The distance to keep
    pub fn with_distance(self, distance: Float) -> Self {
        self.joint.distance = distance;
        self
    }

    /// Sets the pivots for the distance joint
    ///
    /// The pivot is a relative translation from the center of the Body the
    // joint attaches to.
    ///
    /// ## Arguments
    ///
    /// * 'pivot1' - The new pivot for body1
    /// * 'pivot2' - The new pivot for body2
    pub fn with_pivots(self, pivot1: Vector2, pivot2: Vector2) -> Self {
        self.joint.pivot1 = pivot1;
        self.joint.pivot2 = pivot2;
        self
    }
}
