use crate::collision::*;
use crate::physics::Body;
use crate::prelude::*;

/// Helper for building new Body structs
pub struct BodyBuilder<'a> {
    body: &'a mut Body,
    colliders: &'a mut Vec<Collider>,
}

impl<'a> BodyBuilder<'a> {
    pub(crate) fn new(body: &'a mut Body, colliders: &'a mut Vec<Collider>) -> BodyBuilder<'a> {
        Self { body, colliders }
    }

    /// Finishes building the Body and returns its unique ID
    pub fn finish(self) -> usize {
        self.body.id
    }

    /// Sets the position of the Body
    ///
    /// ## Arguments
    ///
    /// * 'position' - The new position
    pub fn with_position(self, position: Vector2) -> Self {
        self.body.transform.position = position;
        self
    }

    /// Sets the velocity of the Body
    ///
    /// ## Arguments
    ///
    /// * 'velocity' - The new velocity
    pub fn with_velocity(self, velocity: Vector2) -> Self {
        self.body.velocity.linear = velocity;
        self
    }

    /// Sets the rotation of the Body
    ///
    /// ## Arguments
    ///
    /// * 'rotation' - The new rotation
    pub fn with_rotation(self, rotation: Float) -> Self {
        self.body.transform.rotation = rotation;
        self
    }

    /// Sets the angular velocity of the Body
    ///
    /// ## Arguments
    ///
    /// * 'angular_velocity' - The new angular velocity
    pub fn with_angular_velocity(self, angular_velocity: Float) -> Self {
        self.body.velocity.angular = angular_velocity;
        self
    }

    /// Sets the ratio of gravity influencing the Body
    ///
    /// ## Arguments
    ///
    /// * 'gravity_influence' - The new gravity multiplier
    pub fn with_gravity_influence(self, gravity_influence: Float) -> Self {
        self.body.gravity_influence = gravity_influence;
        self
    }

    /// Sets whether the Body is static i.e. immovable
    ///
    /// ## Arguments
    ///
    /// * 'is_static' - Whether the Body is marked static
    pub fn with_is_static(self, is_static: bool) -> Self {
        self.body.is_static = is_static;
        self
    }

    /// Sets the mass of the Body
    ///
    /// ## Arguments
    ///
    /// * 'is_static' - The new mass of the Body
    pub fn with_mass(self, mass: Float) -> Self {
        self.body.material.set_mass(mass);
        self
    }

    /// Sets the inertia of the Body
    ///
    /// ## Arguments
    ///
    /// * 'is_static' - The new inertia of the Body
    pub fn with_inertia(self, inertia: Float) -> Self {
        self.body.material.set_inertia(inertia);
        self
    }

    /// Sets the restitution of the Body
    ///
    /// ## Arguments
    ///
    /// * 'is_static' - The new restitution of the Body
    pub fn with_restitution(self, restitution: Float) -> Self {
        self.body.material.restitution = restitution;
        self
    }

    /// Sets the friction of the Body
    ///
    /// ## Arguments
    ///
    /// * 'is_static' - The new friction of the Body
    pub fn with_friction(self, friction: Float) -> Self {
        self.body.material.friction = friction;
        self
    }

    /// Adds a CircleCollider to the Body
    ///
    /// ## Arguments
    ///
    /// * 'radius' - The radius of the CircleCollider
    pub fn with_circle(self, radius: Float) -> Self {
        let collider = Collider::Circle(CircleCollider {
            body: self.body.id,
            position: Vector2::zero(),
            radius,
        });

        self.colliders.push(collider);
        self
    }

    /// Adds a CapsuleCollider to the Body
    ///
    /// ## Arguments
    ///
    /// * 'height' - The height of the CapsuleCollider
    /// * 'radius' - The radius of the CapsuleCollider
    pub fn with_capsule(self, height: Float, radius: Float) -> Self {
        let collider = Collider::Capsule(CapsuleCollider {
            body: self.body.id,
            position: Vector2::zero(),
            rotation: 0.0,
            height,
            radius,
        });

        self.colliders.push(collider);
        self
    }

    /// Adds a BoxCollider to the Body
    ///
    /// ## Arguments
    ///
    /// * 'extents' - The extents of the BoxCollider
    pub fn with_box(self, extents: Vector2) -> Self {
        let collider = Collider::Box(BoxCollider {
            body: self.body.id,
            position: Vector2::zero(),
            rotation: 0.0,
            extents,
        });

        self.colliders.push(collider);
        self
    }

    /// Adds a PolygonCollider to the Body
    ///
    /// ## Arguments
    ///
    /// * 'vertices' - The vertices of the PolygonCollider
    pub fn with_polygon(self, vertices: Vec<Vector2>) -> Self {
        let collider = Collider::Polygon(PolygonCollider {
            body: self.body.id,
            position: Vector2::zero(),
            rotation: 0.0,
            vertices,
        });

        self.colliders.push(collider);
        self
    }
}
