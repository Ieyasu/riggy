use super::{Vector2, AABB};

use crate::prelude::Float;

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Line {
    pub start: Vector2,
    pub end: Vector2,
}

impl Line {
    pub fn new(start: Vector2, end: Vector2) -> Self {
        Self { start, end }
    }

    pub fn center(&self) -> Vector2 {
        0.5 * (self.end - self.start)
    }

    pub fn length(&self) -> Float {
        (self.start - self.end).length()
    }

    pub fn bounds(&self) -> AABB {
        AABB::new(self.start.min(self.end), self.start.max(self.end))
    }
}
