use std::ops::*;

use crate::prelude::*;

pub trait HasCrossProduct<T = Self> {
    type Output;

    fn cross(&self, rhs: T) -> Self::Output;
}

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Vector2 {
    pub x: Float,
    pub y: Float,
}

impl Vector2 {
    pub const fn zero() -> Self {
        Self { x: 0.0, y: 0.0 }
    }

    pub const fn one() -> Self {
        Self { x: 1.0, y: 1.0 }
    }

    pub fn new(x: Float, y: Float) -> Self {
        Self { x, y }
    }

    pub fn abs(&self) -> Vector2 {
        Vector2::new(self.x.abs(), self.y.abs())
    }

    pub fn clamp(&self, min: Vector2, max: Vector2) -> Vector2 {
        Vector2::new(self.x.clamp(min.x, max.x), self.y.clamp(min.y, max.y))
    }

    pub fn dot(&self, rhs: Vector2) -> Float {
        self.x * rhs.x + self.y * rhs.y
    }

    pub fn length(&self) -> Float {
        self.length_sqr().sqrt()
    }

    pub fn length_sqr(&self) -> Float {
        self.x * self.x + self.y * self.y
    }

    pub fn min(&self, rhs: Vector2) -> Vector2 {
        Vector2 {
            x: self.x.min(rhs.x),
            y: self.y.min(rhs.y),
        }
    }

    pub fn max(&self, rhs: Vector2) -> Vector2 {
        Vector2 {
            x: self.x.max(rhs.x),
            y: self.y.max(rhs.y),
        }
    }

    pub fn clamp_length(&self, max: Float) -> Vector2 {
        const EPSILON: Float = 0.0001;

        let length = self.length();
        if length < EPSILON {
            return *self;
        }

        let normalized = *self / length;
        normalized * length.clamp(0.0, max)
    }

    pub fn normalized(&self) -> Vector2 {
        const EPSILON: Float = 0.0001;

        let length = self.length();
        if length < EPSILON {
            *self
        } else {
            *self / length
        }
    }

    pub fn lerp(&self, rhs: Self, t: Float) -> Self {
        let x = self.x + t * (rhs.x - self.x);
        let y = self.y + t * (rhs.y - self.y);
        Self::new(x, y)
    }
}

impl HasCrossProduct for Vector2 {
    type Output = Float;

    fn cross(&self, rhs: Vector2) -> Self::Output {
        self.x * rhs.y - self.y * rhs.x
    }
}

impl HasCrossProduct<&Vector2> for Vector2 {
    type Output = Float;

    fn cross(&self, rhs: &Self) -> Self::Output {
        self.x * rhs.y - self.y * rhs.x
    }
}

impl HasCrossProduct for &Vector2 {
    type Output = Float;

    fn cross(&self, rhs: Self) -> Self::Output {
        self.x * rhs.y - self.y * rhs.x
    }
}

impl HasCrossProduct<Vector2> for &Vector2 {
    type Output = Float;

    fn cross(&self, rhs: Vector2) -> Self::Output {
        self.x * rhs.y - self.y * rhs.x
    }
}

impl HasCrossProduct<Float> for Vector2 {
    type Output = Self;

    fn cross(&self, rhs: Float) -> Self::Output {
        Vector2::new(self.y * rhs, self.x * -rhs)
    }
}

impl HasCrossProduct<Float> for &Vector2 {
    type Output = Vector2;

    fn cross(&self, rhs: Float) -> Self::Output {
        Vector2::new(self.y * rhs, self.x * -rhs)
    }
}

impl HasCrossProduct<Vector2> for Float {
    type Output = Vector2;

    fn cross(&self, rhs: Vector2) -> Self::Output {
        Vector2::new(-self * rhs.y, self * rhs.x)
    }
}

impl Add for Vector2 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Add<&Vector2> for Vector2 {
    type Output = Self;

    fn add(self, rhs: &Self) -> Self::Output {
        Self::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Add for &Vector2 {
    type Output = Vector2;

    fn add(self, rhs: Self) -> Self::Output {
        Vector2::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl Add<Vector2> for &Vector2 {
    type Output = Vector2;

    fn add(self, rhs: Vector2) -> Self::Output {
        Vector2::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl AddAssign for Vector2 {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl AddAssign<&Vector2> for Vector2 {
    fn add_assign(&mut self, rhs: &Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl Sub for Vector2 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl Sub<&Vector2> for Vector2 {
    type Output = Self;

    fn sub(self, rhs: &Self) -> Self::Output {
        Self::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl Sub for &Vector2 {
    type Output = Vector2;

    fn sub(self, rhs: Self) -> Self::Output {
        Vector2::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl Sub<Vector2> for &Vector2 {
    type Output = Vector2;

    fn sub(self, rhs: Vector2) -> Self::Output {
        Vector2::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl SubAssign for Vector2 {
    fn sub_assign(&mut self, rhs: Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl SubAssign<&Vector2> for Vector2 {
    fn sub_assign(&mut self, rhs: &Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl Mul<Float> for Vector2 {
    type Output = Self;

    fn mul(self, rhs: Float) -> Self::Output {
        Self::new(self.x * rhs, self.y * rhs)
    }
}

impl Mul<Float> for &Vector2 {
    type Output = Vector2;

    fn mul(self, rhs: Float) -> Self::Output {
        Vector2::new(self.x * rhs, self.y * rhs)
    }
}

impl Mul<Vector2> for Float {
    type Output = Vector2;

    fn mul(self, rhs: Self::Output) -> Self::Output {
        Vector2::new(self * rhs.x, self * rhs.y)
    }
}

impl Mul<&Vector2> for Float {
    type Output = Vector2;

    fn mul(self, rhs: &Vector2) -> Self::Output {
        Vector2::new(self * rhs.x, self * rhs.y)
    }
}

impl MulAssign<Float> for Vector2 {
    fn mul_assign(&mut self, rhs: Float) {
        self.x *= rhs;
        self.y *= rhs;
    }
}

impl Div<Float> for Vector2 {
    type Output = Self;

    fn div(self, rhs: Float) -> Self::Output {
        Self::new(self.x / rhs, self.y / rhs)
    }
}

impl Div<Float> for &Vector2 {
    type Output = Vector2;

    fn div(self, rhs: Float) -> Self::Output {
        Vector2::new(self.x / rhs, self.y / rhs)
    }
}

impl DivAssign<Float> for Vector2 {
    fn div_assign(&mut self, rhs: Float) {
        self.x /= rhs;
        self.y /= rhs;
    }
}

impl Neg for Vector2 {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self::new(-self.x, -self.y)
    }
}

impl Neg for &Vector2 {
    type Output = Vector2;

    fn neg(self) -> Self::Output {
        Vector2::new(-self.x, -self.y)
    }
}

impl Index<usize> for Vector2 {
    type Output = Float;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            _ => panic!("Index {} out of bounds", index),
        }
    }
}
