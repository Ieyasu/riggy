use super::Vector2;

use crate::prelude::*;

use std::fmt::Display;

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct AABB {
    pub min: Vector2,
    pub max: Vector2,
}

impl AABB {
    pub const fn empty() -> Self {
        Self {
            min: Vector2::zero(),
            max: Vector2::zero(),
        }
    }

    pub fn new(min: Vector2, max: Vector2) -> Self {
        Self { min, max }
    }

    pub fn centered(center: Vector2, extents: Vector2) -> Self {
        Self::new(-extents + center, extents + center)
    }

    pub fn at_origin(extents: Vector2) -> Self {
        Self::centered(Vector2::zero(), extents)
    }

    pub fn center(&self) -> Vector2 {
        0.5 * (self.max + self.min)
    }

    pub fn size(&self) -> Vector2 {
        self.max - self.min
    }

    pub fn extents(&self) -> Vector2 {
        0.5 * self.size()
    }

    pub fn area(&self) -> Float {
        (self.max.x - self.min.x) * (self.max.y - self.min.y)
    }

    pub fn vertices(&self) -> [Vector2; 4] {
        let v0 = self.min;
        let v1 = Vector2::new(self.max.x, self.min.y);
        let v2 = self.max;
        let v3 = Vector2::new(self.min.x, self.max.y);
        [v0, v1, v2, v3]
    }

    pub fn expand(&mut self, other: &AABB) {
        self.min = self.min.min(other.min);
        self.max = self.max.max(other.max);
    }

    pub fn expanded(&self, other: &AABB) -> AABB {
        let min = self.min.min(other.min);
        let max = self.max.max(other.max);
        AABB::new(min, max)
    }

    pub fn try_expand(&mut self, other: &Option<AABB>) {
        match other {
            Some(value) => self.expand(value),
            None => (),
        }
    }

    pub fn try_expanded(&self, other: &Option<AABB>) -> AABB {
        match other {
            Some(value) => self.expanded(value),
            None => *self,
        }
    }
}

impl Display for AABB {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(
            f,
            "AABB((x: {}, y: {}) -> (x: {} ,y: {}))",
            self.min.x, self.min.y, self.max.x, self.max.y
        )
    }
}
