use super::*;

use crate::prelude::*;

#[derive(Debug, Clone, PartialEq)]
pub enum Shape {
    Circle(Circle),
    Capsule(Capsule),
    Polygon(Polygon),
}

impl Shape {
    pub fn new_circle(origin: Vector2, radius: Float) -> Shape {
        Shape::Circle(Circle::new(origin, radius))
    }

    pub fn new_capsule(position: Vector2, rotation: Float, height: Float, radius: Float) -> Shape {
        let half_height = 0.5 * height;
        let delta = half_height * Vector2::new(-rotation.sin(), rotation.cos());
        Shape::Capsule(Capsule::new(position - delta, position + delta, radius))
    }

    pub fn new_polygon(vertices: Vec<Vector2>) -> Shape {
        Shape::Polygon(Polygon::new(vertices))
    }

    pub fn bounds(&self) -> AABB {
        match self {
            Shape::Circle(shape) => shape.bounds(),
            Shape::Capsule(shape) => shape.bounds(),
            Shape::Polygon(shape) => shape.bounds(),
        }
    }
}
