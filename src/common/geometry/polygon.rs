use super::*;

use crate::prelude::Float;

#[derive(Debug, Clone, PartialEq)]
pub struct Polygon {
    pub vertices: Vec<Vector2>,
    pub normals: Vec<Vector2>,
}

impl Polygon {
    pub fn new(vertices: Vec<Vector2>) -> Self {
        let vertex_count = vertices.len();
        let mut poly = Self {
            vertices,
            normals: Vec::with_capacity(vertex_count),
        };
        poly.recalculate_normals();
        poly
    }

    pub fn center(&self) -> Vector2 {
        self.vertices.iter().fold(Vector2::zero(), |a, b| a + b) / self.vertices.len() as Float
    }

    pub fn edge(&self, i: usize) -> Line {
        Line::new(
            self.vertices[i],
            self.vertices[(i + 1) % self.vertices.len()],
        )
    }

    pub fn recalculate_normals(&mut self) {
        self.normals.resize(self.vertices.len(), Vector2::zero());
        for i in 0..self.vertices.len() {
            let v0 = self.vertices[i];
            let v1 = self.vertices[(i + 1) % self.vertices.len()];
            let axis_norm = (v1 - v0).normalized();
            self.normals[i] = Vector2::new(axis_norm.y, -axis_norm.x);
        }
    }

    pub fn area(&self) -> Float {
        let mut area = 0.0;
        for i in 1..self.vertices.len() {
            let a = self.vertices[0];
            let b = self.vertices[i];
            let c = self.vertices[(i + 1) % self.vertices.len()];
            area += 0.5 * (b - a).cross(c - a);
        }
        area
    }

    pub fn bounds(&self) -> AABB {
        let (min, max) = self
            .vertices
            .iter()
            .fold((self.vertices[0], self.vertices[0]), |range, x| {
                (range.0.min(*x), range.1.max(*x))
            });
        AABB::new(min, max)
    }
}

impl From<&AABB> for Polygon {
    fn from(aabb: &AABB) -> Self {
        Polygon::new(aabb.vertices().to_vec())
    }
}
