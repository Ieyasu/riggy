use super::Line;

use crate::prelude::*;

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Capsule {
    pub line: Line,
    pub radius: Float,
}

impl Capsule {
    pub fn new(start: Vector2, end: Vector2, radius: Float) -> Self {
        Self {
            line: Line::new(start, end),
            radius,
        }
    }

    pub fn area(&self) -> Float {
        (PI * self.radius + 2.0 * self.line.length()) * self.radius
    }

    pub fn bounds(&self) -> AABB {
        let r = Vector2 {
            x: self.radius,
            y: self.radius,
        };
        let min_point = self.line.start.min(self.line.end);
        let max_point = self.line.start.max(self.line.end);
        AABB::new(min_point - r, max_point + r)
    }
}
