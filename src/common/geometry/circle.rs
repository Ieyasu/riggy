use super::{Vector2, AABB};

use crate::prelude::*;

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Circle {
    pub origin: Vector2,
    pub radius: Float,
}

impl Circle {
    pub fn new(origin: Vector2, radius: Float) -> Self {
        Self { origin, radius }
    }

    pub fn area(&self) -> Float {
        PI * self.radius * self.radius
    }

    pub fn bounds(&self) -> AABB {
        let r = Vector2::new(self.radius, self.radius);
        AABB::new(self.origin - r, self.origin + r)
    }
}
