pub mod geometry;

pub type Float = f32;
pub const PI: Float = std::f32::consts::PI as Float;
