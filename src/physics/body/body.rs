use super::{Material, Transform, Velocity};

use crate::prelude::*;

/// Represents a rigidbody
///
/// All physical interactions in the simulation require a Body object. To
/// interact with other Bodies, the Body needs a Collider or a joint.
pub struct Body {
    pub material: Material,
    pub transform: Transform,
    pub velocity: Velocity,
    pub(crate) id: usize,
    pub gravity_influence: Float,
    pub is_static: bool,
}

impl Body {
    pub(crate) fn new(id: usize) -> Self {
        Self {
            id,
            material: Material::default(),
            transform: Transform::default(),
            velocity: Velocity::default(),
            gravity_influence: 1.0,
            is_static: false,
        }
    }

    /// Adds an impulse to the Body
    ///
    /// ## Arguments
    ///
    /// * `impulse` - The impulse vector to add to the body
    pub fn add_impulse(&mut self, impulse: Vector2) {
        self.velocity.linear += impulse * self.material.inv_mass;
    }

    /// Adds an impulse to the Body at the given point
    ///
    /// Unlike `add_impulse()`, this function adds rotation to the Body if the
    /// given point is not the center of mass.
    ///
    /// ## Arguments
    ///
    /// * `impulse` - The impulse vector to add to the body
    /// * `point` - The point to add the impulse to (in world coordinates)
    pub fn add_impulse_at_position(&mut self, impulse: Vector2, point: Vector2) {
        let relative_position = point - self.transform.position;
        let angular_impulse = relative_position.cross(impulse);
        self.velocity.linear += impulse * self.material.inv_mass;
        self.velocity.angular += angular_impulse * self.material.inv_inertia;
    }
}
