use crate::prelude::*;

/// Represents the physical material of a Body
///
/// Material contains all properties that describe the physical properties of
/// the body and how they interact with other objects.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Material {
    /// Restitution i.e. bounciness of the Material
    ///
    /// Higher values will make the Body bounce from others on impact.
    pub restitution: Float,

    /// Friction of the Material
    ///
    /// Higher values will slow down the Body when dragging against another
    /// object.
    pub friction: Float,

    pub(crate) inv_mass: Float,
    pub(crate) inv_inertia: Float,
}

impl Default for Material {
    fn default() -> Self {
        Self::new()
    }
}

impl Material {
    pub(crate) fn new() -> Self {
        Self {
            inv_mass: 1.0,
            inv_inertia: 1.0,
            restitution: 0.3,
            friction: 0.5,
        }
    }

    /// Returns the mass of the Body
    pub fn mass(&self) -> Float {
        if self.inv_mass == 0.0 {
            0.0
        } else {
            1.0 / self.inv_mass
        }
    }

    /// Sets the mass of the Body
    ///
    /// ## Arguments
    ///
    /// * `mass` - The new mass
    pub fn set_mass(&mut self, mass: Float) {
        if mass == 0.0 {
            self.inv_mass = 0.0
        } else {
            self.inv_mass = 1.0 / mass
        }
    }

    /// Returns the inertia of the Body
    pub fn inertia(&self) -> Float {
        if self.inv_inertia == 0.0 {
            0.0
        } else {
            1.0 / self.inv_inertia
        }
    }

    /// Sets the inertia of the Body
    ///
    /// ## Arguments
    ///
    /// * `inertia` - The new inertia
    pub fn set_inertia(&mut self, inertia: Float) {
        if inertia == 0.0 {
            self.inv_inertia = 0.0
        } else {
            self.inv_inertia = 1.0 / inertia
        }
    }
}
