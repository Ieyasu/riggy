use crate::prelude::*;

/// Represents the linear and angular velocity of a Body
#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Velocity {
    /// The linear velocity of the Body
    pub linear: Vector2,

    /// The angular velocity of the Body
    pub angular: Float,
}
