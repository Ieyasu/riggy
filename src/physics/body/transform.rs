use crate::prelude::*;

/// Represents the positional and rotational transform of a Body
#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Transform {
    /// The position of the Body
    pub position: Vector2,

    /// The rotation of the Body
    pub rotation: Float,
}

impl Transform {
    /// Returns a point transformed from the local coordinate system of the
    /// Body to the world coordinate system
    ///
    /// ## Arguments
    ///
    /// * `point` - The point to transform
    pub fn local_to_world(&self, point: Vector2) -> Vector2 {
        let s = self.rotation.sin();
        let c = self.rotation.cos();
        self.position + Vector2::new(point.x * c - point.y * s, point.x * s + point.y * c)
    }
}
