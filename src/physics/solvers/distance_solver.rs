use crate::physics::{Body, DistanceJoint};
use crate::prelude::*;

use rayon::prelude::*;

struct CachedJoint {
    pub body1: usize,
    pub body2: usize,
    pub pivot1: Vector2,
    pub pivot2: Vector2,
    pub normal: Vector2,
    pub impulse: Float,
    pub distance: Float,
    pub effective_mass: Float,
    pub softness_k: Float,
    pub bias: Float,
}

pub(crate) struct DistanceSolver {
    joint_cache: Vec<CachedJoint>,
}

impl DistanceSolver {
    const BAUMGARTE_SCALAR: Float = 0.1; // [0.1, 0.3]
    const BAUMGARTE_SLOP: Float = 0.005; // [0.001, 0.01]

    pub fn new() -> Self {
        Self {
            joint_cache: Vec::new(),
        }
    }

    pub fn initialize(&mut self, dt: Float, joints: &[DistanceJoint], bodies: &[Body]) {
        let cache_iter = joints
            .par_iter()
            .map(|joint| Self::get_cached_data(dt, joint, bodies));
        self.joint_cache.clear();
        self.joint_cache.par_extend(cache_iter);
    }

    pub fn solve(&mut self, bodies: &mut [Body]) {
        for joint in self.joint_cache.iter_mut() {
            Self::solve_joint(joint, bodies)
        }
    }

    fn get_cached_data(dt: Float, joint: &DistanceJoint, bodies: &[Body]) -> CachedJoint {
        let body1 = &bodies[joint.body1];
        let body2 = &bodies[joint.body2];
        let world_pivot1 = body1.transform.local_to_world(joint.pivot1);
        let world_pivot2 = body2.transform.local_to_world(joint.pivot2);

        // Calculate joint normal
        let delta = world_pivot2 - world_pivot1;
        let distance = delta.length();
        let normal = if distance > 0.0 {
            delta / distance
        } else {
            Vector2::new(1.0, 0.0)
        };

        // Calculate effective mass affecting the constraint
        let rn1 = joint.pivot1.cross(normal);
        let rn2 = joint.pivot2.cross(normal);
        let inv_effective_mass = body1.material.inv_mass
            + body2.material.inv_mass
            + body1.material.inv_inertia * rn1 * rn1
            + body2.material.inv_inertia * rn2 * rn2;

        let effective_mass = if inv_effective_mass > 0.0 {
            1.0 / inv_effective_mass
        } else {
            0.0
        };

        // Calculate spring coefficients from frequency and damping ratio
        let stifness = effective_mass * joint.frequency * joint.frequency;
        let damping = 2.0 * effective_mass * joint.damping * joint.frequency;

        // Calculate constraint parameters from spring coefficients
        let baumgarte_div = damping + dt * stifness;
        let baumgarte_scalar = if baumgarte_div > 0.0 {
            stifness / baumgarte_div
        } else {
            Self::BAUMGARTE_SCALAR
        };

        let softness_k = dt * (damping + dt * stifness);
        let softness_k = if softness_k > 0.0 {
            1.0 / softness_k
        } else {
            0.0
        };

        let offset = distance - joint.distance;
        let offset = if offset > Self::BAUMGARTE_SLOP {
            offset - Self::BAUMGARTE_SLOP
        } else if offset < -Self::BAUMGARTE_SLOP {
            offset + Self::BAUMGARTE_SLOP
        } else {
            0.0
        };

        let inv_effective_mass = inv_effective_mass + softness_k;
        let effective_mass = if inv_effective_mass > 0.0 {
            1.0 / inv_effective_mass
        } else {
            0.0
        };

        let bias = offset * baumgarte_scalar;

        CachedJoint {
            body1: joint.body1,
            body2: joint.body2,
            pivot1: world_pivot1 - body1.transform.position,
            pivot2: world_pivot2 - body2.transform.position,
            normal,
            effective_mass,
            distance,
            softness_k,
            bias,
            impulse: 0.0,
        }
    }

    fn solve_joint(joint: &mut CachedJoint, bodies: &mut [Body]) {
        let body1 = &bodies[joint.body1];
        let body2 = &bodies[joint.body2];
        let velocity1 = body1.velocity.linear + body1.velocity.angular.cross(joint.pivot1);
        let velocity2 = body2.velocity.linear + body2.velocity.angular.cross(joint.pivot2);
        let rel_vel = velocity2 - velocity1;

        // Calculate impulse
        let impulse = (-rel_vel.dot(joint.normal) - joint.bias - joint.impulse * joint.softness_k)
            * joint.effective_mass;
        joint.impulse += impulse;
        let impulse_vec = impulse * joint.normal;

        // Apply impulses
        let body1 = &mut bodies[joint.body1];
        if !body1.is_static {
            let angular_impulse = joint.pivot1.cross(impulse_vec);
            body1.velocity.linear -= impulse_vec * body1.material.inv_mass;
            body1.velocity.angular -= angular_impulse * body1.material.inv_inertia;
        }

        let body2 = &mut bodies[joint.body2];
        if !body2.is_static {
            let angular_impulse = joint.pivot2.cross(impulse_vec);
            body2.velocity.linear += impulse_vec * body2.material.inv_mass;
            body2.velocity.angular += angular_impulse * body2.material.inv_inertia;
        }
    }
}
