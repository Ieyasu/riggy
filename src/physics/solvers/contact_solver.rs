use crate::physics::{Body, Contact};
use crate::prelude::*;

use rayon::prelude::*;

struct CachedContact {
    pub points: [CachedContactPoint; 2],
    pub normal: Vector2,
    pub body1: usize,
    pub body2: usize,
    pub friction: Float,
    pub restitution: Float,
    pub point_count: u8,
}

#[derive(Default)]
struct CachedContactPoint {
    pub rel_pos: [Vector2; 2],
    pub bias: Float,
    pub normal_mass: Float,
    pub normal_impulse: Float,
    pub tangent_mass: Float,
    pub tangent_impulse: Float,
}

pub(crate) struct ContactSolver {
    contact_cache: Vec<CachedContact>,
}

impl ContactSolver {
    const BAUMGARTE_SCALAR: Float = 0.1; // [0.1, 0.3]
    const BAUMGARTE_SLOP: Float = 0.005; // [0.001, 0.01]

    pub fn new() -> Self {
        ContactSolver {
            contact_cache: Vec::new(),
        }
    }

    pub fn initialize(&mut self, dt: Float, contacts: &[Contact], bodies: &mut [Body]) {
        let cache_iter = contacts
            .par_iter()
            .map(|col| Self::get_cached_data(dt, col, bodies));
        self.contact_cache.clear();
        self.contact_cache.par_extend(cache_iter);
    }

    pub fn solve(&mut self, bodies: &mut [Body]) {
        for col in self.contact_cache.iter_mut() {
            Self::solve_contact(col, bodies)
        }
    }

    fn get_cached_data(dt: Float, contact: &Contact, bodies: &[Body]) -> CachedContact {
        let body1 = &bodies[contact.body1];
        let body2 = &bodies[contact.body2];

        // Static objects have a mass and inertia of zero
        let (inv_mass1, inv_inertia1) = if body1.is_static {
            (0.0, 0.0)
        } else {
            (body1.material.inv_mass, body1.material.inv_inertia)
        };
        let (inv_mass2, inv_inertia2) = if body2.is_static {
            (0.0, 0.0)
        } else {
            (body2.material.inv_mass, body2.material.inv_inertia)
        };

        let friction = Self::mix_friction(body1.material.friction, body2.material.friction);
        let restitution =
            Self::mix_restitution(body1.material.restitution, body2.material.restitution);

        let normal = contact.normal;
        let tangent = Vector2::new(normal.y, -normal.x);
        let penetration = (contact.penetration - Self::BAUMGARTE_SLOP).max(0.0);
        let penetration_bias = penetration * Self::BAUMGARTE_SCALAR / dt;

        let mut contact_points = [CachedContactPoint::default(), CachedContactPoint::default()];
        for m in 0..contact.contact_count as usize {
            let contact_point = &mut contact_points[m];
            let rel_pos1 = contact.contact_points[m] - body1.transform.position;
            let rel_pos2 = contact.contact_points[m] - body2.transform.position;
            let total_velocity1 = body1.velocity.linear + body1.velocity.angular.cross(rel_pos1);
            let total_velocity2 = body2.velocity.linear + body2.velocity.angular.cross(rel_pos2);
            let rel_vel = (total_velocity2 - total_velocity1).dot(normal);

            let rn1 = rel_pos1.cross(normal);
            let rn2 = rel_pos2.cross(normal);
            let normal_mass_div =
                inv_mass1 + inv_mass2 + inv_inertia1 * rn1 * rn1 + inv_inertia2 * rn2 * rn2;

            let rt1 = rel_pos1.cross(tangent);
            let rt2 = rel_pos2.cross(tangent);
            let tangent_mass_div =
                inv_mass1 + inv_mass2 + inv_inertia1 * rt1 * rt1 + inv_inertia2 * rt2 * rt2;

            contact_point.rel_pos[0] = rel_pos1;
            contact_point.rel_pos[1] = rel_pos2;
            contact_point.normal_mass = if normal_mass_div > 0.0 {
                1.0 / normal_mass_div
            } else {
                0.0
            };
            contact_point.tangent_mass = if tangent_mass_div > 0.0 {
                1.0 / tangent_mass_div
            } else {
                0.0
            };

            // Make sure restitution is only applied if we are moving towards each other
            let restitution_bias = if rel_vel < -1.0 {
                restitution * rel_vel
            } else {
                0.0
            };

            contact_point.bias = -penetration_bias + restitution_bias;
        }

        CachedContact {
            points: contact_points,
            normal: contact.normal,
            body1: contact.body1,
            body2: contact.body2,
            friction,
            restitution,
            point_count: contact.contact_count,
        }
    }

    fn solve_contact(contact: &mut CachedContact, bodies: &mut [Body]) {
        let normal = contact.normal;
        let tangent = Vector2::new(normal.y, -normal.x);

        let body1 = &bodies[contact.body1];
        let body2 = &bodies[contact.body2];
        let mut velocity1 = body1.velocity.linear;
        let mut velocity2 = body2.velocity.linear;
        let mut angular_velocity1 = body1.velocity.angular;
        let mut angular_velocity2 = body2.velocity.angular;

        let (inv_mass1, inv_inertia1) = if body1.is_static {
            (0.0, 0.0)
        } else {
            (body1.material.inv_mass, body1.material.inv_inertia)
        };
        let (inv_mass2, inv_inertia2) = if body2.is_static {
            (0.0, 0.0)
        } else {
            (body2.material.inv_mass, body2.material.inv_inertia)
        };

        // Solve friction separately for extra stability
        for m in 0..contact.point_count as usize {
            let contact_point = &mut contact.points[m];
            let total_velocity1 = velocity1 + angular_velocity1.cross(contact_point.rel_pos[0]);
            let total_velocity2 = velocity2 + angular_velocity2.cross(contact_point.rel_pos[1]);
            let rel_vel = total_velocity2 - total_velocity1;

            // Calculate friction impulse
            let friction = -rel_vel.dot(tangent) * contact_point.tangent_mass;

            // Make sure to clamp accumulated friction instead of the current
            let max_friction = contact_point.normal_impulse * contact.friction;
            let old_friction = contact_point.tangent_impulse;
            let new_friction = (old_friction + friction).clamp(-max_friction, max_friction);
            contact_point.tangent_impulse = new_friction;
            let friction = new_friction - old_friction;
            let impulse_vec = friction * tangent;

            // Apply impulses
            let angular_impulse = contact_point.rel_pos[0].cross(impulse_vec);
            velocity1 -= impulse_vec * inv_mass1;
            angular_velocity1 -= angular_impulse * inv_inertia1;

            let angular_impulse = contact_point.rel_pos[1].cross(impulse_vec);
            velocity2 += impulse_vec * inv_mass2;
            angular_velocity2 += angular_impulse * inv_inertia2;
        }

        for m in 0..contact.point_count as usize {
            let contact_point = &mut contact.points[m];
            let total_velocity1 = velocity1 + angular_velocity1.cross(contact_point.rel_pos[0]);
            let total_velocity2 = velocity2 + angular_velocity2.cross(contact_point.rel_pos[1]);
            let rel_vel = (total_velocity2 - total_velocity1).dot(normal);

            // Calculate normal impulse
            let impulse = -(rel_vel + contact_point.bias) * contact_point.normal_mass;

            // Make sure to clamp accumulated impulse instead of the current
            let old_impulse = contact_point.normal_impulse;
            let new_impulse = (old_impulse + impulse).max(0.0);
            contact_point.normal_impulse = new_impulse;
            let impulse = new_impulse - old_impulse;
            let impulse_vec = impulse * normal;

            // Apply impulses
            let angular_impulse = contact_point.rel_pos[0].cross(impulse_vec);
            velocity1 -= impulse_vec * inv_mass1;
            angular_velocity1 -= angular_impulse * inv_inertia1;

            let angular_impulse = contact_point.rel_pos[1].cross(impulse_vec);
            velocity2 += impulse_vec * inv_mass2;
            angular_velocity2 += angular_impulse * inv_inertia2;
        }

        let body1 = &mut bodies[contact.body1];
        body1.velocity.linear = velocity1;
        body1.velocity.angular = angular_velocity1;

        let body2 = &mut bodies[contact.body2];
        body2.velocity.linear = velocity2;
        body2.velocity.angular = angular_velocity2;
    }

    fn mix_friction(friction1: Float, friction2: Float) -> Float {
        (friction1 * friction2).sqrt()
    }

    fn mix_restitution(restitution1: Float, restitution2: Float) -> Float {
        restitution1.max(restitution2)
    }
}
