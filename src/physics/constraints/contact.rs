use crate::collision::query::Manifold;
use crate::prelude::*;

#[doc(hidden)]
pub struct Contact {
    pub contact_points: [Vector2; 2],
    pub body1: usize,
    pub body2: usize,
    pub normal: Vector2,
    pub penetration: Float,
    pub contact_count: u8,
}

impl Contact {
    pub fn new(body1: usize, body2: usize, manifold: Manifold) -> Self {
        debug_assert_ne!(body1, body2);

        Self {
            body1,
            body2,
            normal: manifold.normal,
            penetration: manifold.penetration,
            contact_count: manifold.contact_count,
            contact_points: manifold.contact_points,
        }
    }
}
