use crate::prelude::*;

/// A joint that keeps two Bodies at a fixed distance from each other
///
/// The joint can also simulate a spring with the `frequency` and `damping`
/// fields.
pub struct DistanceJoint {
    pub(crate) id: usize,
    pub(crate) body1: usize,
    pub(crate) body2: usize,

    /// The relative offset from the center of the first Body
    pub pivot1: Vector2,

    /// The relative offset from the center of the second Body
    pub pivot2: Vector2,

    /// The distance to enforce between the two Bodies
    pub distance: Float,

    /// The oscillation frequency of the joint.
    ///
    /// If set to zero, the joint will be perfectly rigid.
    pub frequency: Float,

    /// The oscillation damping of the joint.
    pub damping: Float,
}

impl DistanceJoint {
    pub(crate) fn new(id: usize) -> Self {
        Self {
            id,
            body1: 0,
            body2: 0,
            pivot1: Vector2::zero(),
            pivot2: Vector2::zero(),
            distance: 0.0,
            frequency: 0.0,
            damping: 0.0,
        }
    }

    // Returns the unique ID of the joint
    pub fn id(&self) -> usize {
        self.id
    }

    // Returns the first body the joint is connected to
    pub fn body1(&self) -> usize {
        self.body1
    }

    // Returns the second body the joint is connected to
    pub fn body2(&self) -> usize {
        self.body2
    }
}
