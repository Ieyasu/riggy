use crate::physics::solvers::*;
use crate::physics::{Body, Contact, DistanceJoint};
use crate::prelude::*;

pub(crate) struct ConstraintSystem {
    pub(crate) joints: Vec<DistanceJoint>,
    iterations: usize,
    contact_solver: ContactSolver,
    distance_solver: DistanceSolver,
}

impl ConstraintSystem {
    pub fn new() -> Self {
        ConstraintSystem {
            iterations: 10,
            joints: Vec::new(),
            contact_solver: ContactSolver::new(),
            distance_solver: DistanceSolver::new(),
        }
    }

    pub fn update(&mut self, dt: Float, contact: &[Contact], bodies: &mut [Body]) {
        self.contact_solver.initialize(dt, contact, bodies);
        self.distance_solver.initialize(dt, &self.joints, bodies);

        for _ in 0..self.iterations {
            self.contact_solver.solve(bodies);
            self.distance_solver.solve(bodies);
        }
    }
}
