use crate::physics::Body;
use crate::prelude::*;

use rayon::prelude::*;

pub(crate) struct GravitySystem {
    pub gravity: Vector2,
}

impl GravitySystem {
    pub fn new(gravity: Vector2) -> Self {
        Self { gravity }
    }

    pub fn update(&mut self, dt: Float, bodies: &mut [Body]) {
        bodies.par_iter_mut().for_each(|body| {
            if !body.is_static {
                body.velocity.linear += self.gravity * dt * body.gravity_influence;
            }
        });
    }
}
