use crate::physics::Body;
use crate::prelude::*;

use rayon::prelude::*;

pub(crate) struct MovementSystem {}

impl MovementSystem {
    pub fn new() -> Self {
        Self {}
    }

    pub fn update(&mut self, dt: Float, bodies: &mut [Body]) {
        bodies.par_iter_mut().for_each(|body| {
            debug_assert_eq!(
                true,
                !body.is_static || body.velocity.linear == Vector2::zero()
            );

            body.transform.position += body.velocity.linear * dt;
            body.transform.rotation += body.velocity.angular * dt;
        });
    }
}
