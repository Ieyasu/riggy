#![feature(iterator_fold_self)]
#![feature(iter_partition_in_place)]

pub mod collision;
pub mod common;
pub mod physics;
pub mod prelude;
pub mod world;
