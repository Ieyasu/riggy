use crate::collision::query;
use crate::collision::CollisionShape;
use crate::physics::Contact;
use rayon::prelude::*;

pub(crate) struct NarrowPhaseSystem {
    pub contacts: Vec<Contact>,
}

impl NarrowPhaseSystem {
    pub fn new() -> Self {
        Self {
            contacts: Vec::new(),
        }
    }

    pub fn update(&mut self, shapes: &[CollisionShape], contacts: &[(usize, usize)]) {
        let contacts_iter = contacts.par_iter().filter_map(|contact| {
            let col_shape1 = &shapes[contact.0];
            let col_shape2 = &shapes[contact.1];
            let manifold = query::manifold::shape_shape(&col_shape1.value, &col_shape2.value);
            match manifold {
                Some(m) => Some(Contact::new(col_shape1.body, col_shape2.body, m)),
                None => None,
            }
        });

        self.contacts.clear();
        self.contacts.par_extend(contacts_iter);

        log::info!("Narrow phase contacts {} kpl", self.contacts.len());
    }
}
