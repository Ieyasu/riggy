use crate::collision::bvh::*;
use crate::collision::CollisionShape;

pub(crate) struct BroadPhaseSystem {
    pub bvh: Bvh,
    pub contacts: Vec<(usize, usize)>,
    builder: BvhBuilder,
    traverser: BvhTraverser,
}

impl BroadPhaseSystem {
    pub fn new() -> Self {
        Self {
            bvh: Bvh::new(),
            builder: BvhBuilder::new(),
            traverser: BvhTraverser::new(),
            contacts: Vec::new(),
        }
    }

    pub fn update(&mut self, shapes: &[CollisionShape]) {
        self.build(shapes);
        log::info!("BVH nodes {} kpl", self.bvh.nodes.len());
        self.traverse();
        log::info!("Broad phase contacts {} kpl", self.contacts.len());
    }

    fn build(&mut self, shapes: &[CollisionShape]) {
        self.builder.build(shapes, &mut self.bvh);
    }

    fn traverse(&mut self) {
        self.traverser.traverse(&self.bvh, &mut self.contacts)
    }
}
