use crate::collision::*;
use crate::common::geometry::Shape;
use crate::physics::Body;
use crate::prelude::*;
use rayon::prelude::*;

/// A system that transforms Colliders into a CollisionShapes.
///
/// The main motivation is to keep the fields of Collider in a user friendly
/// format while Shape is optimized for collision detection. While the
/// Transform of Collider is relative to its body, all Shape data is
/// already in world space to speed up collision checks.
pub(crate) struct ShapeSystem {
    pub shapes: Vec<CollisionShape>,
}

impl ShapeSystem {
    pub fn new() -> Self {
        Self { shapes: Vec::new() }
    }

    pub fn update(&mut self, colliders: &[Collider], bodies: &[Body]) {
        let shapes_iter = colliders.par_iter().map(|col| match col {
            Collider::Circle(col) => Self::create_circle(&bodies[col.body], col),
            Collider::Capsule(col) => Self::create_capsule(&bodies[col.body], col),
            Collider::Box(col) => Self::create_box(&bodies[col.body], col),
            Collider::Polygon(col) => Self::create_polygon(&bodies[col.body], col),
        });

        self.shapes.clear();
        self.shapes.par_extend(shapes_iter);
    }

    fn create_circle(body: &Body, col: &CircleCollider) -> CollisionShape {
        CollisionShape::new(
            body.id,
            Shape::new_circle(body.transform.local_to_world(col.position), col.radius),
        )
    }

    fn create_capsule(body: &Body, col: &CapsuleCollider) -> CollisionShape {
        let position = body.transform.local_to_world(col.position);
        let rotation = body.transform.rotation + col.rotation;
        CollisionShape::new(
            body.id,
            Shape::new_capsule(position, rotation, col.height, col.radius),
        )
    }

    fn create_box(body: &Body, col: &BoxCollider) -> CollisionShape {
        let center = body.transform.local_to_world(col.position);
        let rotation = body.transform.rotation + col.rotation;
        let cos = rotation.cos();
        let sin = rotation.sin();
        let x = Vector2::new(cos, sin) * col.extents.x;
        let y = Vector2::new(-sin, cos) * col.extents.y;
        let vertices = vec![
            center - x - y,
            center + x - y,
            center + x + y,
            center - x + y,
        ];
        CollisionShape::new(body.id, Shape::new_polygon(vertices))
    }

    fn create_polygon(body: &Body, col: &PolygonCollider) -> CollisionShape {
        let center = body.transform.local_to_world(col.position);
        let rotation = body.transform.rotation + col.rotation;
        let cos = rotation.cos();
        let sin = rotation.sin();
        let vertices = col.vertices.iter().map(|vert| {
            Vector2::new(
                cos * vert.x - sin * vert.y + center.x,
                sin * vert.x + cos * vert.y + center.y,
            )
        });

        CollisionShape::new(body.id, Shape::new_polygon(vertices.collect()))
    }
}
