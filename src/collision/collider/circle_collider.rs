use crate::prelude::*;

/// A circle shaped collider
pub struct CircleCollider {
    pub(crate) body: usize,

    // The center position of the CircleCollider
    ///
    /// The position is relative to the Body that owns the collider.
    pub position: Vector2,

    /// Radius of the CircleCollider
    pub radius: Float,
}
