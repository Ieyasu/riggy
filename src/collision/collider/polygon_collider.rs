use crate::prelude::*;

/// A convex polygon shaped collider
pub struct PolygonCollider {
    pub(crate) body: usize,

    // The center position of the PolygonCollider
    ///
    /// The position is relative to the Body that owns the collider.
    pub position: Vector2,

    /// Rotation of the PolygonCollider (in radians)
    ///
    /// The rotation is relative to the Body that owns the collider.
    pub rotation: Float,

    /// Vector containing the vertices of the polygon
    ///
    /// The vertices must be in clockwise order and form a convex shape.
    pub vertices: Vec<Vector2>,
}
