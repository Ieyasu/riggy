use crate::prelude::*;

/// A box shaped collider
pub struct BoxCollider {
    pub(crate) body: usize,

    // The center position of the BoxCollider
    ///
    /// The position is relative to the Body that owns the collider.
    pub position: Vector2,

    // The rotation of the BoxCollider (in radians)
    ///
    /// The rotation is relative to the Body that owns the collider.
    pub rotation: Float,

    // The extents (half size) of the Box Collider
    pub extents: Vector2,
}
