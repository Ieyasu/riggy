use crate::prelude::*;

/// A capsule shaped collider
pub struct CapsuleCollider {
    pub(crate) body: usize,

    /// Center of the CapsuleCollider
    ///
    /// The position is relative to the Body that owns the collider.
    pub position: Vector2,

    /// Rotation of the CapsuleCollider (in radians)
    ///
    /// The rotation is relative to the Body that owns the collider.
    pub rotation: Float,

    /// Height of the CapsuleCollider
    pub height: Float,

    /// Radius of the CapsuleCollider
    pub radius: Float,
}
