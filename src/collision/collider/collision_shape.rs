use crate::common::geometry::Shape;

#[doc(hidden)]
pub struct CollisionShape {
    pub body: usize,
    pub value: Shape,
}

impl CollisionShape {
    pub(crate) fn new(body: usize, value: Shape) -> Self {
        Self { body, value }
    }
}
