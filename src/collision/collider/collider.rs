use super::*;

/// An enum representing all available Collider shapes
///
/// The available shapes are in order from most performant to least performant:
/// * CircleCollider
/// * CapsuleCollider
/// * BoxCollider
/// * PolygonCollider
pub enum Collider {
    /// A circle shaped collider
    Circle(CircleCollider),

    /// A capsule shaped collider
    Capsule(CapsuleCollider),

    /// A box shaped collider
    Box(BoxCollider),

    /// A convex polygon shaped collider
    Polygon(PolygonCollider),
}

impl Collider {
    /// Gets the CircleCollider representation or panics incase the Collider is
    /// not a CircleCollider
    pub fn to_circle(&self) -> &CircleCollider {
        if let Collider::Circle(col) = self {
            col
        } else {
            panic!("Not a circle")
        }
    }

    /// Gets the mutable CircleCollider representation or panics incase the
    // Collider is not a CircleCollider
    pub fn to_circle_mut(&mut self) -> &mut CircleCollider {
        if let Collider::Circle(col) = self {
            col
        } else {
            panic!("Not a circle")
        }
    }

    /// Gets the CapsuleCollider representation or panics incase the Collider is
    /// not a CapsuleCollider
    pub fn to_capsule(&self) -> &CapsuleCollider {
        if let Collider::Capsule(col) = self {
            col
        } else {
            panic!("Not a capsule")
        }
    }

    /// Gets the mutable CapsuleCollider representation or panics incase the
    // Collider is not a CapsuleCollider
    pub fn to_capsule_mut(&mut self) -> &mut CapsuleCollider {
        if let Collider::Capsule(col) = self {
            col
        } else {
            panic!("Not a capsule")
        }
    }

    /// Gets the BoxCollider representation or panics incase the Collider is
    /// not a BoxCollider
    pub fn to_box(&self) -> &BoxCollider {
        if let Collider::Box(col) = self {
            col
        } else {
            panic!("Not a box")
        }
    }

    /// Gets the mutable BoxCollider representation or panics incase the
    // Collider is not a BoxCollider
    pub fn to_box_mut(&mut self) -> &mut BoxCollider {
        if let Collider::Box(col) = self {
            col
        } else {
            panic!("Not a box")
        }
    }

    /// Gets the PolygonCollider representation or panics incase the Collider is
    /// not a PolygonCollider
    pub fn to_poly(&self) -> &PolygonCollider {
        if let Collider::Polygon(col) = self {
            col
        } else {
            panic!("Not a polygon")
        }
    }

    /// Gets the mutable PolygonCollider representation or panics incase the
    // Collider is not a PolygonCollider
    pub fn to_poly_mut(&mut self) -> &mut PolygonCollider {
        if let Collider::Polygon(col) = self {
            col
        } else {
            panic!("Not a polygon")
        }
    }
}
