use super::intersects;

use crate::common::geometry::*;
use crate::prelude::*;

pub fn point_line(point: &Vector2, line: &Line) -> Vector2 {
    point_line_points(point, &line.start, &line.end)
}

pub fn point_line_points(point: &Vector2, start: &Vector2, end: &Vector2) -> Vector2 {
    let ab = end - start;
    let t = (point - start).dot(ab) / ab.length_sqr();
    start + ab * t.clamp(0.0, 1.0)
}

pub fn point_circle(point: &Vector2, circle: &Circle) -> Vector2 {
    let delta = circle.origin - point;
    delta.clamp_length(circle.radius)
}

pub fn point_capsule(point: &Vector2, capsule: &Capsule) -> Vector2 {
    let delta = point - point_line(point, &capsule.line);
    delta.clamp_length(capsule.radius)
}

pub fn point_aabb(point: &Vector2, aabb: &AABB) -> Vector2 {
    point.clamp(aabb.min, aabb.max)
}

pub fn point_poly(point: &Vector2, poly: &Polygon) -> Vector2 {
    // Check point against each edge of the polygon
    let mut min_distance_sqr = Float::MAX;
    let mut closest_point = *point;
    for i in 0..poly.vertices.len() {
        // If point is "behind" the edge, no need to check distance
        // This means either the point is inside the polygon or not closest to
        // this edge.
        let v0 = poly.vertices[i];
        let normal = poly.normals[i];
        if normal.dot((point - v0).normalized()) < 0.0 {
            continue;
        }

        // Get the point distance to the edge
        let v1 = poly.vertices[(i + 1) % poly.vertices.len()];
        let candidate_point = point_line(point, &Line::new(v0, v1));
        let distance_sqr = (candidate_point - point).length_sqr();
        if distance_sqr < min_distance_sqr {
            min_distance_sqr = distance_sqr;
            closest_point = candidate_point;
        }
    }

    closest_point
}

pub fn line_line(line1: &Line, line2: &Line) -> (Vector2, Vector2) {
    const EPSILON: Float = 0.0001;

    let d1 = line1.end - line1.start;
    let d2 = line2.end - line2.start;
    let r = line1.start - line2.start;

    let d1_len_sqr = d1.dot(d1);
    let d2_len_sqr = d2.dot(d2);
    let f = d2.dot(r);

    let mut t1;
    let mut t2;

    if d1_len_sqr <= EPSILON && d2_len_sqr <= EPSILON {
        return (line1.start, line2.start);
    }

    if d1_len_sqr <= EPSILON {
        t1 = 0.0;
        t2 = f / d2_len_sqr;
        t2 = t2.clamp(0.0, 1.0);
    } else {
        let c = d1.dot(r);

        if d2_len_sqr <= EPSILON {
            t2 = 0.0;
            t1 = (-c / d1_len_sqr).clamp(0.0, 1.0);
        } else {
            let b = d1.dot(d2);
            let b2 = b * b;
            let denom = d1_len_sqr * d2_len_sqr - b2;

            if denom != 0.0 {
                t1 = ((b * f - c * d2_len_sqr) / denom).clamp(0.0, 1.0);
            } else {
                t1 = 0.0;
            }

            t2 = (b * t1 + f) / d2_len_sqr;
            if t2 < 0.0 {
                t2 = 0.0;
                t1 = (-c / d1_len_sqr).clamp(0.0, 1.0);
            } else if t2 > 1.0 {
                t2 = 1.0;
                t1 = ((b - c) / d1_len_sqr).clamp(0.0, 1.0);
            }
        }
    }

    (line1.start + d1 * t1, line2.start + d2 * t2)
}

pub fn line_circle(line: &Line, circle: &Circle) -> (Vector2, Vector2) {
    let point_on_line = point_line(&circle.origin, line);
    let point_on_circle = (point_on_line - circle.origin).clamp_length(circle.radius);

    (point_on_line, point_on_circle)
}

pub fn line_capsule(line: &Line, capsule: &Capsule) -> (Vector2, Vector2) {
    let (point1, point2) = line_line(line, &capsule.line);
    let point_on_line = point1;
    let point_on_capsule = (point1 - point2).clamp_length(capsule.radius);

    (point_on_line, point_on_capsule)
}

pub fn line_aabb(line: &Line, aabb: &AABB) -> (Vector2, Vector2) {
    // Check if a line point is inside the AABB
    let closest_point = point_line(&aabb.center(), line);
    if intersects::point_aabb(&closest_point, aabb) {
        return (closest_point, closest_point);
    }

    // Check line against each edge of the AABB
    let vertices = aabb.vertices();
    let mut min_distance_sqr = Float::MAX;
    let mut closest_point1 = Vector2::zero();
    let mut closest_point2 = Vector2::zero();
    for i in 0..4 {
        let v0 = vertices[i];
        let v1 = vertices[(i + 1) % vertices.len()];
        let (point1, point2) = line_line(line, &Line::new(v0, v1));

        if point1 == point2 {
            return (point1, point1);
        }

        let distance_sqr = (point2 - point1).length_sqr();
        if distance_sqr < min_distance_sqr {
            min_distance_sqr = distance_sqr;
            closest_point1 = point1;
            closest_point2 = point2;
        }
    }

    (closest_point1, closest_point2)
}

pub fn line_poly(line: &Line, poly: &Polygon) -> (Vector2, Vector2) {
    // Check if center is inside the polygon
    let center = line.center();
    let closest_point = point_poly(&center, &poly);
    if closest_point == center {
        return (center, center);
    }

    // Check line against each edge of the polygon
    let mut min_distance_sqr = Float::MAX;
    let mut closest_point1 = center;
    let mut closest_point2 = center;
    for i in 0..poly.vertices.len() {
        let v0 = poly.vertices[i];
        let v1 = poly.vertices[(i + 1) % poly.vertices.len()];
        let (point1, point2) = line_line(line, &Line::new(v0, v1));

        if point1 == point2 {
            return (point1, point1);
        }

        let distance_sqr = (point1 - point2).length_sqr();
        if distance_sqr < min_distance_sqr {
            min_distance_sqr = distance_sqr;
            closest_point1 = point1;
            closest_point2 = point2;
        }
    }

    (closest_point1, closest_point2)
}
