use super::distance;

use crate::common::geometry::*;
use crate::prelude::*;

pub fn point_circle(point: &Vector2, circle: &Circle) -> bool {
    let radius = circle.radius;
    let distance_sqr = distance::point_point_sqr(point, circle.origin);

    distance_sqr < radius * radius
}

pub fn point_capsule(point: &Vector2, capsule: &Capsule) -> bool {
    let radius = capsule.radius;
    let distance_sqr = distance::point_line_sqr(point, &capsule.line);

    distance_sqr < radius * radius
}

pub fn point_aabb(point: &Vector2, aabb: &AABB) -> bool {
    point.x <= aabb.max.x && point.x >= aabb.min.x && point.y <= aabb.max.y && point.y >= aabb.min.y
}

pub fn point_poly(_point: &Vector2, _poly: &Polygon) -> bool {
    todo!()
}

pub fn line_circle(line: &Line, circle: &Circle) -> bool {
    let radius = circle.radius;
    let distance_sqr = distance::point_line_sqr(&circle.origin, line);

    distance_sqr < radius * radius
}

pub fn line_capsule(line: &Line, capsule: &Capsule) -> bool {
    let radius = capsule.radius;
    let distance_sqr = distance::line_line_sqr(line, &capsule.line);

    distance_sqr < radius * radius
}

pub fn line_aabb(line: &Line, aabb: &AABB) -> bool {
    const EPSILON: Float = 0.0001;

    let aabb_size = aabb.size();
    let line_dir = line.end - line.start;
    let line_origin = line.end + line.start - aabb.max - aabb.min;
    let mut line_abs = line_dir.abs();

    for i in 0..2 {
        if line_origin[i].abs() > aabb_size[i] + line_abs[i] {
            return false;
        }
    }

    line_abs += Vector2::new(EPSILON, EPSILON);

    let cross = (line_origin.x * line_dir.y - line_origin.y * line_dir.x).abs();
    let axis = aabb_size.x * line_abs.y + aabb_size.y * line_abs.x;
    if cross > axis {
        return false;
    }

    true
}

pub fn line_poly(_line: &Line, _poly: &Polygon) -> bool {
    todo!()
}

// pub fn circle_circle(
//         circle_origin1: Vector2, circle_radius1: Float,
//         circle_origin2: Vector2, circle_radius2: Float) -> bool {

//     let radius = circle_radius1 + circle_radius2;
//     let distance_sqr = distance::point_point_sqr(circle_origin1, circle_origin2);

//     distance_sqr < radius * radius
// }

// pub fn circle_capsule(
//         circle_origin: Vector2, circle_radius: Float,
//         line_start: Vector2, line_end: Vector2, line_radius: Float) -> bool {

//     let radius = circle_radius + line_radius;
//     let distance_sqr = distance::point_line_sqr(circle_origin, line_start, line_end);

//     distance_sqr < radius * radius
// }

// pub fn circle_aabb(circle_origin: Vector2, circle_radius: Float, aabb_min: Vector2, aabb_max: Vector2) -> bool {
//     let radius = circle_radius;
//     let distance_sqr = distance::point_aabb_sqr(circle_origin, aabb_min, aabb_max);

//     distance_sqr < radius * radius
// }

// pub fn capsule_capsule(
//         cap_start1: Vector2, cap_end1: Vector2, cap_radius1: Float,
//         cap_start2: Vector2, cap_end2: Vector2, cap_radius2: Float) -> bool {

//     let radius = cap_radius1 + cap_radius2;
//     let distance_sqr = distance::line_line_sqr(cap_start1, cap_end1, cap_start2, cap_end2);

//     distance_sqr < radius * radius
// }

// pub fn capsule_aabb(
//         cap_start1: Vector2, cap_end1: Vector2, cap_radius1: Float,
//         aabb_min: Vector2, aabb_max: Vector2) -> bool {

//     let radius = cap_radius1;
//     let distance_sqr = distance::line_aabb_sqr(cap_start1, cap_end1, aabb_min, aabb_max);

//     distance_sqr < radius * radius
// }

pub fn aabb_aabb(aabb1: &AABB, aabb2: &AABB) -> bool {
    let delta1 = aabb2.min - aabb1.max;
    if delta1.x > 0.0 || delta1.y > 0.0 {
        return false;
    }

    let delta2 = aabb1.min - aabb2.max;
    delta2.x <= 0.0 && delta2.y <= 0.0
}
