use super::{closest_point, distance};

use crate::common::geometry::*;
use crate::prelude::*;

const EPSILON: Float = 0.0001;

pub struct Manifold {
    pub contact_points: [Vector2; 2],
    pub normal: Vector2,
    pub penetration: Float,
    pub contact_count: u8,
}

impl Manifold {
    pub fn new(contact_point: Vector2, normal: Vector2, penetration: Float) -> Self {
        let contact_points = [contact_point, Vector2::zero()];
        Self {
            contact_points,
            normal,
            penetration,
            contact_count: 1,
        }
    }

    pub fn new_two(
        contact_points: [Vector2; 2],
        contact_count: u8,
        normal: Vector2,
        penetration: Float,
    ) -> Manifold {
        Manifold {
            contact_points,
            normal,
            penetration,
            contact_count,
        }
    }
}

pub fn shape_shape(shape1: &Shape, shape2: &Shape) -> Option<Manifold> {
    match (shape1, shape2) {
        (Shape::Circle(a), Shape::Circle(b)) => circle_circle(a, b),
        (Shape::Circle(a), Shape::Capsule(b)) => circle_capsule(a, b),
        (Shape::Circle(a), Shape::Polygon(b)) => circle_poly(a, b),
        (Shape::Capsule(a), Shape::Circle(b)) => flip(&circle_capsule(b, a)),
        (Shape::Capsule(a), Shape::Capsule(b)) => capsule_capsule(a, b),
        (Shape::Capsule(a), Shape::Polygon(b)) => capsule_poly(a, b),
        (Shape::Polygon(a), Shape::Circle(b)) => flip(&circle_poly(b, a)),
        (Shape::Polygon(a), Shape::Capsule(b)) => flip(&capsule_poly(b, a)),
        (Shape::Polygon(a), Shape::Polygon(b)) => poly_poly(a, b),
    }
}

fn flip(manifold: &Option<Manifold>) -> Option<Manifold> {
    match manifold {
        Some(m) => Some(Manifold::new_two(
            m.contact_points,
            m.contact_count,
            -m.normal,
            m.penetration,
        )),
        None => None,
    }
}

pub fn circle_circle(circle1: &Circle, circle2: &Circle) -> Option<Manifold> {
    let delta = circle2.origin - circle1.origin;
    let distance_sqr = delta.length_sqr();
    let radius = circle1.radius + circle2.radius;

    if distance_sqr >= radius * radius {
        return None;
    }

    let distance = distance_sqr.sqrt();
    let normal: Vector2;
    if distance < EPSILON {
        normal = Vector2::new(0.0, 1.0);
    } else {
        normal = delta / distance;
    }

    let penetration = radius - distance;
    let contact_point = circle1.origin + normal * circle1.radius;
    Some(Manifold::new(contact_point, normal, penetration))
}

pub fn circle_capsule(circle: &Circle, capsule: &Capsule) -> Option<Manifold> {
    let line = capsule.line;
    let point = closest_point::point_line(&circle.origin, &line);
    let delta = point - circle.origin;
    let distance_sqr = delta.length_sqr();
    let radius = circle.radius + capsule.radius;

    if distance_sqr >= radius * radius {
        return None;
    }

    let distance = distance_sqr.sqrt();
    let normal: Vector2;
    if distance < EPSILON {
        let capsule = line.end - line.start;
        normal = Vector2::new(capsule.y, -capsule.x).normalized();
    } else {
        normal = delta / distance;
    }

    let penetration = radius - distance;
    let contact_point = circle.origin + normal * circle.radius;
    Some(Manifold::new(contact_point, normal, penetration))
}

pub fn circle_poly(circle: &Circle, poly: &Polygon) -> Option<Manifold> {
    let mut min_distance_sqr = Float::MAX;
    let mut min_edge = 0;
    let mut is_inside = true;
    let mut next_vert = *poly.vertices.first().unwrap();

    // Find the polygon edge with the smallest distance to the circle
    for (i, vert) in poly.vertices.iter().enumerate().rev() {
        let is_behind = poly.normals[i].dot(circle.origin - vert) < 0.0;
        is_inside = is_inside && is_behind;

        // Closest point calculation is deferred until we know which edge is closest
        let distance_sqr = distance::point_line_points_sqr(&circle.origin, &vert, &next_vert);
        if distance_sqr < min_distance_sqr {
            min_distance_sqr = distance_sqr;
            min_edge = i;
        }

        next_vert = *vert;
    }

    // Circle origin inside the polygon
    if is_inside {
        let normal = -poly.normals[min_edge];
        let penetration = circle.radius + min_distance_sqr.sqrt();
        let contact_point = circle.origin + normal * circle.radius;
        return Some(Manifold::new(contact_point, normal, penetration));
    }

    // No intersection
    if min_distance_sqr >= circle.radius * circle.radius {
        return None;
    }

    // Circle origin outside the polygon
    if min_distance_sqr < EPSILON {
        let normal = -poly.normals[min_edge];
        let penetration = circle.radius;
        let contact_point = circle.origin + normal * circle.radius;
        Some(Manifold::new(contact_point, normal, penetration))
    } else {
        let v0 = poly.vertices[min_edge];
        let v1 = poly.vertices[(min_edge + 1) % poly.vertices.len()];
        let closest_point = closest_point::point_line_points(&circle.origin, &v0, &v1);
        let distance = min_distance_sqr.sqrt();
        let normal = (closest_point - circle.origin) / distance;
        let penetration = circle.radius - distance;
        let contact_point = circle.origin + normal * circle.radius;
        Some(Manifold::new(contact_point, normal, penetration))
    }
}

pub fn capsule_capsule(capsule1: &Capsule, capsule2: &Capsule) -> Option<Manifold> {
    let (point1, point2) = closest_point::line_line(&capsule1.line, &capsule2.line);
    let delta = point2 - point1;
    let distance_sqr = delta.length_sqr();
    let radius = capsule1.radius + capsule2.radius;

    if distance_sqr >= radius * radius {
        return None;
    }

    let distance = distance_sqr.sqrt();
    let normal = if distance < EPSILON {
        let capsule = capsule1.line.end - capsule2.line.start;
        Vector2::new(-capsule.y, capsule.x).normalized()
    } else {
        delta / distance
    };

    let penetration = radius - distance;
    let contact_point = point1 + normal * capsule1.radius;
    Some(Manifold::new(contact_point, normal, penetration))
}

pub fn capsule_poly(cap: &Capsule, poly: &Polygon) -> Option<Manifold> {
    let mut closest_point = Vector2::zero();
    let mut min_distance_sqr = Float::MAX;
    let mut min_delta = Vector2::zero();
    let mut min_edge = 0;
    let mut is_inside_start = true;
    let mut is_inside_end = true;
    let mut next_vert = *poly.vertices.first().unwrap();

    // Find the polygon edge with the smallest distance to the capsule
    for (i, vert) in poly.vertices.iter().enumerate().rev() {
        let is_behind_start = poly.normals[i].dot(cap.line.start - vert) < 0.0;
        let is_behind_end = poly.normals[i].dot(cap.line.end - vert) < 0.0;
        is_inside_start = is_inside_start && is_behind_start;
        is_inside_end = is_inside_end && is_behind_end;

        let edge = Line::new(*vert, next_vert);
        let (point1, point2) = closest_point::line_line(&cap.line, &edge);
        let delta = point2 - point1;
        let distance_sqr = delta.length_sqr();
        if distance_sqr < min_distance_sqr {
            closest_point = point1;
            min_distance_sqr = distance_sqr;
            min_delta = delta;
            min_edge = i;
        }

        next_vert = *vert;
    }

    let edge_normal = poly.normals[min_edge];

    // Both ends inside the polygon
    if is_inside_start && is_inside_end {
        // Find the point that is deeper
        let end_point = if (cap.line.end - closest_point).length_sqr() < EPSILON {
            cap.line.start
        } else {
            cap.line.end
        };

        let end_delta = end_point - closest_point;
        let distance = min_distance_sqr.sqrt();
        let normal = -edge_normal;
        let penetration = cap.radius + end_delta.dot(normal) + distance;
        let contact_point = closest_point + normal * cap.radius;
        return Some(Manifold::new(contact_point, normal, penetration));
    }

    // One end inside the polygon
    if is_inside_start || is_inside_end {
        let inside_point = if is_inside_start {
            cap.line.start
        } else {
            cap.line.end
        };
        let normal = -edge_normal;

        // The penetration is the line segment inside the polygon projected to
        // the normal of the polygon
        let penetration = cap.radius + (inside_point - closest_point).dot(normal);
        let contact_point = inside_point + normal * cap.radius;
        return Some(Manifold::new(contact_point, normal, penetration));
    }

    // No intersection
    if min_distance_sqr >= cap.radius * cap.radius {
        return None;
    }

    // Both ends outside the polygon
    if min_distance_sqr < EPSILON {
        let normal = -poly.normals[min_edge];
        let penetration = cap.radius;
        let contact_point = closest_point + normal * cap.radius;
        Some(Manifold::new(contact_point, normal, penetration))
    } else {
        let distance = min_distance_sqr.sqrt();
        let normal = min_delta / distance;
        let penetration = cap.radius - distance;
        let contact_point = closest_point + normal * cap.radius;
        Some(Manifold::new(contact_point, normal, penetration))
    }
}

pub fn poly_poly(poly1: &Polygon, poly2: &Polygon) -> Option<Manifold> {
    // Find the face with most penetration on collider A
    let (face1, penetration1) = find_face_of_least_penetration(&poly1, &poly2);
    if penetration1 > 0.0 {
        return None;
    }

    // Find the face with most penetration on collider B
    let (face2, penetration2) = find_face_of_least_penetration(&poly2, &poly1);
    if penetration2 > 0.0 {
        return None;
    }

    // Choose reference face from the polygon that has the greater axis of penetration.
    // To avoid instabilities, we need to choose the reference face with bias so that
    // it won't oscillate when both penetrations are equal.
    let flip;
    let ref_face;
    let ref_poly;
    let inc_poly;
    if biased_greater_than(penetration1, penetration2) {
        ref_face = face1;
        ref_poly = poly1;
        inc_poly = poly2;
        flip = false;
    } else {
        ref_face = face2;
        ref_poly = poly2;
        inc_poly = poly1;
        flip = true;
    }

    // Find the colliding face on the incidence polygon
    let ref_normal = ref_poly.normals[ref_face];
    let ref_start = ref_poly.vertices[ref_face];
    let ref_end = ref_poly.vertices[(ref_face + 1) % ref_poly.vertices.len()];
    let inc_face = find_incident_face(inc_poly, ref_normal);
    let inc_start = inc_poly.vertices[inc_face];
    let inc_end = inc_poly.vertices[(inc_face + 1) % inc_poly.vertices.len()];

    // Clip away points that do not line up with the reference polygon
    let refc = ref_normal.dot(ref_start);
    let ref_tangent = Vector2::new(-ref_normal.y, ref_normal.x);
    let negside = -ref_tangent.dot(ref_start);
    let posside = ref_tangent.dot(ref_end);
    let (sp, inc_start, inc_end) = clip(-ref_tangent, negside, inc_start, inc_end);
    if sp < 2 {
        return None;
    }
    let (sp, inc_start, inc_end) = clip(ref_tangent, posside, inc_start, inc_end);
    if sp < 2 {
        return None;
    }

    let normal = if flip { -ref_normal } else { ref_normal };

    // Cull points that aren't behind the reference face
    let mut penetration = 0.0;
    let mut contact_points = [Vector2::zero(), Vector2::zero()];
    let mut contact_count = 0;

    let penetration1 = ref_normal.dot(inc_start) - refc;
    if penetration1 <= 0.0 {
        contact_points[contact_count] = inc_start;
        penetration = -penetration1;
        contact_count += 1;
    }

    let penetration2 = ref_normal.dot(inc_end) - refc;
    if penetration2 <= 0.0 {
        contact_points[contact_count] = inc_end;
        penetration += -penetration2;
        contact_count += 1;
        penetration /= contact_count as Float;
    }

    // Find the contact points from the colliding faces using clipping
    Some(Manifold::new_two(
        contact_points,
        contact_count as u8,
        normal,
        penetration,
    ))
}

fn biased_greater_than(lhs: Float, rhs: Float) -> bool {
    const BIAS_RELATIVE: Float = 0.95;
    const BIAS_ABSOLUTE: Float = 0.01;
    lhs >= rhs * BIAS_RELATIVE + lhs * BIAS_ABSOLUTE
}

fn find_face_of_least_penetration(poly1: &Polygon, poly2: &Polygon) -> (usize, Float) {
    let mut max_distance = Float::MIN;
    let mut face = 0;

    for i in 0..poly1.normals.len() {
        let normal1 = poly1.normals[i];
        let support_vertex = get_support_vertex(&poly2, -normal1);

        let vertex1 = poly1.vertices[i];
        let distance = normal1.dot(support_vertex - vertex1);

        if distance > max_distance {
            max_distance = distance;
            face = i;
        }
    }

    (face, max_distance)
}

fn get_support_vertex(poly: &Polygon, normal: Vector2) -> Vector2 {
    let mut max_projection = Float::MIN;
    let mut support_vertex = Vector2::zero();

    for vertex in poly.vertices.iter() {
        let projection = vertex.dot(normal);
        if projection > max_projection {
            max_projection = projection;
            support_vertex = *vertex;
        }
    }

    support_vertex
}

fn find_incident_face(inc_poly: &Polygon, ref_normal: Vector2) -> usize {
    let mut min_dot = Float::MAX;
    let mut incident_face = 0;

    for i in 0..inc_poly.vertices.len() {
        let dot = ref_normal.dot(inc_poly.normals[i]);
        if dot < min_dot {
            min_dot = dot;
            incident_face = i;
        }
    }

    incident_face
}

fn clip(
    tangent: Vector2,
    side: Float,
    vert1: Vector2,
    vert2: Vector2,
) -> (usize, Vector2, Vector2) {
    let mut sp = 0;
    let mut out = [vert1, vert2];

    let d1 = tangent.dot(vert1) - side;
    let d2 = tangent.dot(vert2) - side;

    if d1 <= 0.0 {
        out[sp] = vert1;
        sp += 1;
    }

    if d2 <= 0.0 {
        out[sp] = vert2;
        sp += 1;
    }

    if d1 * d2 < 0.0 {
        let alpha = d1 / (d1 - d2);
        out[sp] = vert1 + alpha * (vert2 - vert1);
        sp += 1;
    }

    (sp, out[0], out[1])
}
