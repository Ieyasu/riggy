use super::closest_point;

use crate::common::geometry::*;
use crate::prelude::*;

pub fn point_point_sqr(point1: &Vector2, point2: Vector2) -> Float {
    (point1 - point2).length_sqr()
}

pub fn point_line_sqr(point: &Vector2, line: &Line) -> Float {
    point_line_points_sqr(point, &line.start, &line.end)
}

pub fn point_line_points_sqr(point: &Vector2, start: &Vector2, end: &Vector2) -> Float {
    let ab = end - start;
    let ac = point - start;
    let bc = point - end;

    let e = ac.dot(ab);
    if e <= 0.0 {
        return ac.length_sqr();
    }

    let f = ab.length_sqr();
    if e >= f {
        return bc.length_sqr();
    }

    ac.length_sqr() - e * e / f
}

pub fn point_circle_sqr(point: &Vector2, circle: &Circle) -> Float {
    let closest_point = closest_point::point_circle(point, circle);
    (point - closest_point).length_sqr()
}

pub fn point_capsule_sqr(point: &Vector2, capsule: &Capsule) -> Float {
    let closest_point = closest_point::point_capsule(point, capsule);
    (point - closest_point).length_sqr()
}

pub fn point_aabb_sqr(point: &Vector2, aabb: &AABB) -> Float {
    let closest_point = closest_point::point_aabb(point, aabb);
    (point - closest_point).length_sqr()
}

pub fn point_poly_sqr(_point: &Vector2, _poly: &Polygon) -> Float {
    todo!()
}

pub fn line_line_sqr(line1: &Line, line2: &Line) -> Float {
    let (point1, point2) = closest_point::line_line(line1, line2);
    (point1 - point2).length_sqr()
}

pub fn line_circle(line: &Line, circle: &Circle) -> Float {
    let distance = point_line_sqr(&circle.origin, line).sqrt();
    (distance - circle.radius).max(0.0)
}

pub fn line_capsule(line: &Line, capsule: &Capsule) -> Float {
    let distance = line_line_sqr(line, &capsule.line).sqrt();
    (distance - capsule.radius).max(0.0)
}

pub fn line_aabb_sqr(line: &Line, aabb: &AABB) -> Float {
    let distance_sqr = point_aabb_sqr(&line.start, aabb);
    if distance_sqr == 0.0 {
        return 0.0;
    }

    let corner1 = aabb.min;
    let corner2 = Vector2::new(aabb.min.x, aabb.max.y);
    let distance_sqr = distance_sqr.min(line_line_sqr(line, &Line::new(corner1, corner2)));
    if distance_sqr == 0.0 {
        return 0.0;
    }

    let corner3 = aabb.max;
    let distance_sqr = distance_sqr.min(line_line_sqr(line, &Line::new(corner2, corner3)));
    if distance_sqr == 0.0 {
        return 0.0;
    }

    let corner4 = Vector2::new(aabb.max.x, aabb.min.y);
    let distance_sqr = distance_sqr.min(line_line_sqr(line, &Line::new(corner3, corner4)));
    if distance_sqr == 0.0 {
        return 0.0;
    }

    distance_sqr.min(line_line_sqr(line, &Line::new(corner4, corner1)))
}

pub fn line_poly_sqr(_line: &Line, _poly: &Polygon) -> Float {
    todo!()
}
