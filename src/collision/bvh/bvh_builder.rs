use super::{Axis, BinArray, Bvh, BvhAABB, BvhNode};

use crate::collision::CollisionShape;
use crate::prelude::*;

use rayon::prelude::*;
use std::sync::{Arc, Mutex};

pub(crate) struct BvhBuilder {
    centroids: Vec<Vector2>,
    nodes_buffer: Arc<Mutex<Vec<BvhNode>>>,
}

impl Default for BvhBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl BvhBuilder {
    pub(super) const BIN_COUNT: usize = 32;
    pub(super) const LEAF_CAPACITY: usize = 32;

    pub fn new() -> Self {
        Self {
            centroids: Vec::new(),
            nodes_buffer: Arc::new(Mutex::new(Vec::new())),
        }
    }

    pub fn build(&mut self, shapes: &[CollisionShape], bvh: &mut Bvh) {
        if shapes.len() == 0 {
            bvh.nodes.clear();
            return;
        }

        // Precalculate aabbs and centroids
        self.nodes_buffer.lock().unwrap().clear();
        self.update_aabbs(shapes, &mut bvh.aabbs);
        self.update_centroids(&bvh.aabbs);

        // Recursively build the BVH
        let root_aabb = &self.calculate_root_aabb(&bvh.aabbs);
        let root = self.build_node(root_aabb, 0, &mut bvh.aabbs);

        // Reorder nodes for faster travelling due to cache coherency
        self.reorder_nodes(&root, bvh);
    }

    fn build_node(&self, aabb: &AABB, offset: usize, shape_aabbs: &mut [BvhAABB]) -> BvhNode {
        if shape_aabbs.len() < Self::LEAF_CAPACITY {
            return self.create_leaf_node(aabb, offset, shape_aabbs.len());
        }

        // Find the best split for the shapes
        let mut bins_x = self.find_bin_index(aabb, shape_aabbs, Axis::X);
        let mut bins_y = self.find_bin_index(aabb, shape_aabbs, Axis::Y);
        let mut bins = if bins_x.split_cost < bins_y.split_cost {
            &mut bins_x
        } else {
            &mut bins_y
        };

        // If the cost is not improved by splitting using SAH, fallback to
        // median split on the largest axis
        let parent_cost = aabb.area() * shape_aabbs.len() as Float;
        if bins.split_cost >= parent_cost {
            bins = if aabb.size().x > aabb.size().y {
                &mut bins_x
            } else {
                &mut bins_y
            };
            self.find_median_split(shape_aabbs, &mut bins);
        }

        // Reorder the indices so that the ones belonging to the left node are
        // on the left side of the array.
        let split_index = shape_aabbs.iter_mut().partition_in_place(|s| {
            self.calculate_bin_index(aabb, bins.axis, s) < bins.split_index
        });

        // In the worst case all shapes are in one bin. Give up and make a leaf node.
        if split_index == 0 || split_index == shape_aabbs.len() {
            return self.create_leaf_node(aabb, offset, shape_aabbs.len());
        }

        // Calculate AABBs for child nodes
        let l_aabb = bins
            .value
            .iter()
            .take(bins.split_index)
            .filter_map(|x| x.aabb)
            .fold_first(|a, b| a.expanded(&b))
            .unwrap();

        let r_aabb = bins
            .value
            .iter()
            .skip(bins.split_index)
            .filter_map(|x| x.aabb)
            .fold_first(|a, b| a.expanded(&b))
            .unwrap();

        // Recursively build child nodes
        let (l_aabbs, r_aabbs) = shape_aabbs.split_at_mut(split_index);
        let (l_child, r_child) = rayon::join(
            || self.build_node(&l_aabb, offset, l_aabbs),
            || self.build_node(&r_aabb, offset + split_index, r_aabbs),
        );

        self.create_branch_node(aabb, l_child, r_child)
    }

    fn find_bin_index(&self, aabb: &AABB, shape_aabbs: &[BvhAABB], axis: Axis) -> BinArray {
        let mut bins = BinArray::new(axis);

        // Calculate the AABB and primitive count for each bin
        for shape_aabb in shape_aabbs {
            let bin_index = self.calculate_bin_index(aabb, bins.axis, shape_aabb);
            let mut bin = &mut bins.value[bin_index];
            bin.primitive_count += 1;
            bin.aabb = match &bin.aabb {
                Some(value) => Some(value.expanded(&shape_aabb.value)),
                None => Some(shape_aabb.value),
            };
        }

        // Calculate cost of right tree by sweeping from right to left
        let mut running_aabb: Option<AABB> = None;
        let mut running_count = 0;
        for bin in bins.value.iter_mut().rev() {
            running_count += bin.primitive_count;
            running_aabb = match running_aabb {
                Some(value) => Some(value.try_expanded(&bin.aabb)),
                None => bin.aabb,
            };

            bin.right_cost = match running_aabb {
                Some(value) => value.area() * running_count as Float,
                None => 0.0,
            };
        }

        // Calculate cost of left tree and add to cost of right tree by sweeping from left to right
        let mut running_aabb: Option<AABB> = None;
        let mut running_count = 0;
        bins.split_cost = Float::MAX;
        for i in 0..bins.value.len() - 1 {
            running_count += bins.value[i].primitive_count;
            running_aabb = match running_aabb {
                Some(value) => Some(value.try_expanded(&bins.value[i].aabb)),
                None => bins.value[i].aabb,
            };

            let left_cost = match running_aabb {
                Some(value) => value.area() * running_count as Float,
                None => 0.0,
            };

            let cost = left_cost + bins.value[i + 1].right_cost;
            if cost < bins.split_cost {
                bins.split_cost = cost;
                bins.split_index = i + 1;
            }
        }

        bins
    }

    fn find_median_split(&self, shape_aabbs: &[BvhAABB], bins: &mut BinArray) {
        let mut running_count = 0;
        for (i, bin) in bins.value.iter().enumerate() {
            running_count += bin.primitive_count;
            if running_count < shape_aabbs.len() / 2 {
                bins.split_index = i;
            }
        }
    }

    fn update_aabbs(&self, shapes: &[CollisionShape], shape_aabbs: &mut Vec<BvhAABB>) {
        let shape_aabbs_iter = shapes.par_iter().enumerate().map(|(i, shape)| BvhAABB {
            value: shape.value.bounds(),
            index: i,
        });
        shape_aabbs.clear();
        shape_aabbs.par_extend(shape_aabbs_iter);
    }

    fn update_centroids(&mut self, shape_aabbs: &[BvhAABB]) {
        let centroids_iter = shape_aabbs.par_iter().map(|aabb| aabb.value.center());
        self.centroids.clear();
        self.centroids.par_extend(centroids_iter);
    }

    fn calculate_root_aabb(&self, shape_aabbs: &[BvhAABB]) -> AABB {
        shape_aabbs
            .par_iter()
            .map(|aabb| aabb.value)
            .reduce(|| shape_aabbs[0].value, |a, b| a.expanded(&b))
    }

    fn calculate_bin_index(&self, aabb: &AABB, axis: Axis, shape_aabb: &BvhAABB) -> usize {
        let bins_start = aabb.min[axis as usize];
        let bins_size = aabb.size()[axis as usize];
        let centroid = self.centroids[shape_aabb.index][axis as usize];
        let offset = centroid - bins_start;
        ((offset / bins_size * Self::BIN_COUNT as Float) as usize).min(Self::BIN_COUNT - 1)
    }

    fn create_branch_node(&self, parent_aabb: &AABB, lchild: BvhNode, rchild: BvhNode) -> BvhNode {
        // Create a new branch node
        let mut guard = self.nodes_buffer.lock().unwrap();
        guard.push(rchild);
        guard.push(lchild);
        let index1 = guard.len() - 1;
        let index2 = index1 - 1;
        BvhNode::new_branch(*parent_aabb, index1, index2)
    }

    fn create_leaf_node(&self, aabb: &AABB, offset: usize, shape_count: usize) -> BvhNode {
        BvhNode::new_leaf(*aabb, offset, offset + shape_count)
    }

    fn reorder_nodes(&self, root: &BvhNode, bvh: &mut Bvh) {
        const ROOT: u8 = 0;
        const LCHILD: u8 = 1;
        const RCHILD: u8 = 2;

        let mut guard = self.nodes_buffer.lock().unwrap();
        guard.push(*root);

        bvh.nodes.clear();
        let mut stack = vec![(guard.len() - 1, 0, ROOT)];

        while !stack.is_empty() {
            let (old_idx, parent_idx, node_type) = stack.pop().unwrap();
            let new_idx = bvh.nodes.len();
            let node = guard[old_idx];

            // As the node has moved, we need to fix the index of the parent node
            match node_type {
                LCHILD => bvh.nodes[parent_idx].index1 = new_idx,
                RCHILD => bvh.nodes[parent_idx].index2 = new_idx,
                _ => (),
            }

            if !node.is_leaf() {
                stack.push((node.index2, new_idx, RCHILD));
                stack.push((node.index1, new_idx, LCHILD));
            }

            bvh.nodes.push(node);
        }

        assert_eq!(bvh.nodes.len(), guard.len());
    }
}
