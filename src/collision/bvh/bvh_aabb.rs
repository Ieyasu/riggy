use crate::prelude::*;

pub(super) struct BvhAABB {
    pub value: AABB,
    pub index: usize,
}
