use super::{BvhAABB, BvhNode};

#[doc(hidden)]
pub struct Bvh {
    pub nodes: Vec<BvhNode>,
    pub(super) aabbs: Vec<BvhAABB>,
}

impl Bvh {
    pub(crate) fn new() -> Self {
        Bvh {
            nodes: Vec::new(),
            aabbs: Vec::new(),
        }
    }

    /// Returns the root node of the tree
    pub fn root(&self) -> Option<&BvhNode> {
        self.nodes.first()
    }
}
