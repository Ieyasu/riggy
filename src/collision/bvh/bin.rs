use super::{Axis, BvhBuilder};

use crate::prelude::*;

#[derive(Debug, Copy, Clone, PartialEq)]
pub(super) struct Bin {
    pub aabb: Option<AABB>,
    pub right_cost: Float,
    pub primitive_count: usize,
}

pub(super) struct BinArray {
    pub value: [Bin; BvhBuilder::BIN_COUNT],
    pub split_index: usize,
    pub split_cost: Float,
    pub axis: Axis,
}

impl Bin {
    pub fn new() -> Self {
        Self {
            aabb: None,
            right_cost: 0.0,
            primitive_count: 0,
        }
    }
}

impl BinArray {
    pub fn new(axis: Axis) -> Self {
        Self {
            value: [Bin::new(); BvhBuilder::BIN_COUNT],
            split_cost: 0.0,
            axis,
            split_index: 0,
        }
    }
}
