#[derive(Debug, Copy, Clone, PartialEq)]
pub(super) enum Axis {
    X = 0,
    Y = 1,
}
