use crate::prelude::AABB;

#[doc(hidden)]
#[derive(Debug, Copy, Clone)]
pub struct BvhNode {
    pub aabb: AABB,
    pub index1: usize,
    pub index2: usize,
    pub is_leaf: bool,
}

impl PartialEq for BvhNode {
    fn eq(&self, rhs: &Self) -> bool {
        self.index1 == rhs.index1 && self.index2 == rhs.index2
    }
}

impl BvhNode {
    pub(super) fn new_branch(aabb: AABB, index1: usize, index2: usize) -> BvhNode {
        Self {
            aabb,
            index1,
            index2,
            is_leaf: false,
        }
    }

    pub(super) fn new_leaf(aabb: AABB, index1: usize, index2: usize) -> BvhNode {
        Self {
            aabb,
            index1,
            index2,
            is_leaf: true,
        }
    }

    pub fn is_leaf(&self) -> bool {
        self.is_leaf
    }
}
