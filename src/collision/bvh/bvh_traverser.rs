use super::{Bvh, BvhNode};

use crate::collision::query;

use rayon::prelude::*;
use std::cell::RefCell;
use std::sync::{Arc, Mutex};

pub(crate) struct BvhTraverser {}

impl Default for BvhTraverser {
    fn default() -> Self {
        Self::new()
    }
}

impl BvhTraverser {
    pub fn new() -> Self {
        Self {}
    }

    pub fn traverse(&mut self, bvh: &Bvh, pairs: &mut Vec<(usize, usize)>) {
        let pairs_locked = Arc::new(Mutex::new(pairs));
        pairs_locked.lock().unwrap().clear();

        bvh.nodes
            .par_iter()
            .filter(|node| node.is_leaf())
            .for_each(|node| Self::traverse_leaf(node, bvh, &pairs_locked));

        // Sort pairs for determinism
        pairs_locked.lock().unwrap().par_sort_by_key(|a| (a.0, a.1));
    }

    fn traverse_leaf(leaf: &BvhNode, bvh: &Bvh, pairs: &Arc<Mutex<&mut Vec<(usize, usize)>>>) {
        thread_local! {
            static STACK: RefCell<Vec<usize>> = RefCell::new(Vec::new());
            static PAIRS: RefCell<Vec<(usize, usize)>> = RefCell::new(Vec::new());
        }

        PAIRS.with(|p| {
            STACK.with(|s| {
                let p = &mut p.borrow_mut();
                let s = &mut s.borrow_mut();
                Self::traverse_leaf_thread(leaf, bvh, s, p);
                pairs.lock().unwrap().append(p);
            });
        });
    }

    fn traverse_leaf_thread(
        leaf: &BvhNode,
        bvh: &Bvh,
        stack: &mut Vec<usize>,
        pairs: &mut Vec<(usize, usize)>,
    ) {
        debug_assert_eq!(true, leaf.is_leaf());

        // Add contact pairs inside the node itself
        pairs.clear();
        for i in leaf.index1..(leaf.index2 - 1) {
            let aabb1 = &bvh.aabbs[i];
            for j in (i + 1)..leaf.index2 {
                debug_assert_ne!(i, j);
                let aabb2 = &bvh.aabbs[j];
                if query::intersects::aabb_aabb(&aabb1.value, &aabb2.value) {
                    pairs.push((aabb1.index, aabb2.index));
                }
            }
        }

        // Add contact pairs between other nodes
        stack.clear();
        stack.push(0);
        while !stack.is_empty() {
            let node = &bvh.nodes[stack.pop().unwrap()];

            if query::intersects::aabb_aabb(&leaf.aabb, &node.aabb) {
                if node.is_leaf() {
                    if leaf.index1 < node.index1 {
                        Self::traverse_leaf_leaf(leaf, node, bvh, pairs);
                    }
                } else {
                    stack.push(node.index1);
                    stack.push(node.index2);
                }
            }
        }
    }

    fn traverse_leaf_leaf(
        leaf1: &BvhNode,
        leaf2: &BvhNode,
        bvh: &Bvh,
        pairs: &mut Vec<(usize, usize)>,
    ) {
        debug_assert_ne!(leaf1, leaf2);
        debug_assert_eq!(leaf1.is_leaf(), true);
        debug_assert_eq!(leaf2.is_leaf(), true);

        for i in leaf1.index1..leaf1.index2 {
            let aabb1 = &bvh.aabbs[i];
            for j in leaf2.index1..leaf2.index2 {
                debug_assert_ne!(i, j);
                let aabb2 = &bvh.aabbs[j];
                if query::intersects::aabb_aabb(&aabb1.value, &aabb2.value) {
                    pairs.push((aabb1.index, aabb2.index));
                }
            }
        }
    }
}
