# Riggy

Riggy is a fast and minimal 2D physics engine written in rust. The engine follows a data oriented design which enables efficient parallelization and easy(ish) determinism. The core logic is based on the popular sequential impulse resolution method.

![](resources/riggy.webm)

## Features

Riggy is not a fully featured engine. That said, it has the basic building blocks for a physics simulation and can be easily extended with additional constraint/joint types. Current features include:

* Impulse based dynamics
* Friction and bounciness
* Distance-based joints/springs
* Rigidbodies
* Softbodies (via springs)
* Circle/capsule/box/convex polygon colliders
* Queries for intersection/distance/closest point/contact manifold
* Multiple worlds
* Broadphase using a BVH
* Determinism
* Parallel execution
* Several example scenes

## Getting Started

To run the examples, you will need `rust` and `cargo`. Keep in mind that while the core engine itself is small and lightweight, the examples have some large dependencies like [nannou](https://github.com/nannou-org/nannou). Also the rendering can be somewhat slow with large number of objects.

The project uses some nightly features so make sure you have nightly version of rust installed and run the following in the project directory:

```bash
> rustup override set nightly
```

Then run the following command to run the example scene shown above:

```bash
> cargo run --release --example riggy
```

For other example scenes, see the [./examples](examples). directory

## TODO

There are plenty of missing features and nice to haves:

* Additional joint types (primastic, revolution, etc.)
* Motors
* Collision callbacks
* Continuous collision [^1]
* Destructable objects
* Concave polygons (quickhull or decomposition to convex polygons)

In addition there are some low hanging performance/stability optimizations:

* Islands/sleeping bodies
* Warm starting for stability [^2] [^3]
* Parallelized constraint solving

## Sources

[^1] https://box2d.org/files/ErinCatto_ContinuousCollision_GDC2013.pdf

[^2] https://kevinyu.net/2018/02/01/improving-the-stability-of-your-physics/

[^3] https://box2d.org/files/ErinCatto_UnderstandingConstraints_GDC2014.pdf

Other interesting resources:

* About implementation specifics of distance joints in different engines: https://pybullet.org/Bullet/phpBB3/viewtopic.php?t=8888
* Everything from Erin Catto (creator of Box2D) https://box2d.org/publications/
