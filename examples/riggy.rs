mod example;

use example::{ExampleApp, ExampleState};
use rand::Rng;
use riggy::prelude::*;
use std::collections::HashMap;

const WORLD_SIZE: Float = 70.0;
const GRAVITY: Float = 9.81;
const FLOOR_THICKNESS: Float = 1.4;

const TEXT_SCALE: Float = 4.0;
const TEXT_OFFSET: Float = 450.0;
const TEXT_VERTEX_RADIUS: Float = 0.8;

const RIGIDBODY_OFFSET: Float = 140.0;
const CIRCLE_COUNT: usize = 1600;
const CAPSULE_COUNT: usize = 0;
const BOX_COUNT: usize = 0;
const POLYGON_COUNT: usize = 0;

fn main() {
    ExampleApp::run(build);
}

fn build(_app: &nannou::App) -> ExampleState {
    // Create a new world and arbitrary world bounds to help placing stuff
    let world = World::new(World::down() * GRAVITY);
    let mut state = ExampleState::new(world);
    state.draw_scale = 9.1;
    state.world_bounds = AABB::at_origin(Vector2::new(16.0 / 9.0, 1.0) * WORLD_SIZE);

    build_floor(&mut state);

    // Create different shapes of rigidbodies
    for _ in 0..CIRCLE_COUNT {
        let body = build_rigidbody(&mut state);
        let radius = state.rng.gen_range(1.0..=2.0);
        state.world.update_body(body).with_circle(radius);
    }

    for _ in 0..CAPSULE_COUNT {
        let body = build_rigidbody(&mut state);
        let radius = state.rng.gen_range(0.6..=1.0);
        let height = state.rng.gen_range(1.0..=1.4);
        state.world.update_body(body).with_capsule(height, radius);
    }

    for _ in 0..BOX_COUNT {
        let body = build_rigidbody(&mut state);
        let extents = Vector2::new(
            state.rng.gen_range(0.6..=1.2),
            state.rng.gen_range(0.6..=1.2),
        );
        state.world.update_body(body).with_box(extents);
    }

    for _ in 0..POLYGON_COUNT {
        let body = build_rigidbody(&mut state);

        let radius = state.rng.gen_range(1.0..=1.4);
        let vertex_count = state.rng.gen_range(3..=8);
        let vertices = (0..vertex_count).map(|i| {
            let angle = -2.0 * PI * i as Float / vertex_count as Float;
            radius * Vector2::new(angle.sin(), angle.cos())
        });
        state
            .world
            .update_body(body)
            .with_polygon(vertices.collect());
    }

    // Read and create 'RIGGY' text vertices from file
    let offset = state.world.bodies().len() - 1;
    let obj_data = std::include_str!("riggy.obj");
    let mut vertices = HashMap::new();
    for line in obj_data.lines() {
        if line.starts_with("v ") {
            let split = line.split(" ");
            let vertex: Vec<Float> = split
                .skip(1)
                .take(3)
                .map(|x| x.parse().expect("Failed to parse file"))
                .collect();
            assert_eq!(vertex.len(), 3);
            let position =
                TEXT_SCALE * Vector2::new(vertex[0], -vertex[2]) + World::up() * TEXT_OFFSET;
            let body = build_softbody_vertex(&mut state, position, TEXT_VERTEX_RADIUS);
            vertices.insert(body, vertex);
        }
    }

    // Read and create 'RIGGY' edge springs from file
    let mut joints = Vec::new();
    for line in obj_data.lines() {
        if line.starts_with("l") {
            let split = line.split(" ");
            let edge: Vec<usize> = split
                .skip(1)
                .take(2)
                .map(|x| x.parse().expect("Failed to parse file"))
                .collect();
            assert_eq!(edge.len(), 2);
            let id1 = offset + edge[0];
            let id2 = offset + edge[1];
            joints.push((id1, id2));
        }
    }

    for joint in joints {
        // Ugly hack to hide internal joints
        let vertex1 = vertices.get(&joint.0).unwrap();
        let vertex2 = vertices.get(&joint.1).unwrap();
        let draw = vertex1[1] * vertex2[1] > 0.0;
        build_softbody_spring(&mut state, joint.0, joint.1, draw);
    }

    state
}

fn build_floor(state: &mut ExampleState) {
    // Floor
    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(Vector2::new(
            state.world_bounds.center().x,
            state.world_bounds.min.y,
        ))
        .with_restitution(0.4)
        .with_friction(1.0)
        .with_box(Vector2::new(
            state.world_bounds.size().x * 0.5,
            FLOOR_THICKNESS,
        ));

    // Left wall
    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(Vector2::new(
            state.world_bounds.min.x,
            state.world_bounds.center().y,
        ))
        .with_restitution(0.4)
        .with_friction(1.0)
        .with_box(Vector2::new(
            FLOOR_THICKNESS,
            state.world_bounds.size().y * 0.5,
        ));

    // Right wall
    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(Vector2::new(
            state.world_bounds.max.x,
            state.world_bounds.center().y,
        ))
        .with_restitution(0.4)
        .with_friction(1.0)
        .with_box(Vector2::new(
            FLOOR_THICKNESS,
            state.world_bounds.size().y * 0.5,
        ));
}

fn build_rigidbody(state: &mut ExampleState) -> usize {
    state
        .world
        .create_body()
        .with_position(Vector2::new(
            state
                .rng
                .gen_range(state.world_bounds.min.x..=state.world_bounds.max.x),
            state
                .rng
                .gen_range(state.world_bounds.min.y..=4.0 * state.world_bounds.max.y)
                + RIGIDBODY_OFFSET,
        ))
        .with_rotation(state.rng.gen_range(0.0..2.0 * PI))
        .with_restitution(state.rng.gen_range(0.0..0.5))
        .with_friction(state.rng.gen_range(0.1..1.0))
        .with_mass(10.0)
        .with_inertia(10.0)
        .finish()
}

fn build_softbody_vertex(state: &mut ExampleState, position: Vector2, radius: Float) -> usize {
    state
        .world
        .create_body()
        .with_position(position)
        .with_restitution(0.5)
        .with_friction(1.0)
        .with_mass(10.0)
        .with_inertia(10.0)
        .with_circle(radius)
        .finish()
}

fn build_softbody_spring(state: &mut ExampleState, id1: usize, id2: usize, draw: bool) {
    let joint = state
        .world
        .create_distance_joint(id1, id2)
        .with_frequency(1000.0)
        .with_damping(1.0)
        .finish();

    if !draw {
        state.draw_joint_filter.insert(joint);
    }
}
