use nannou::prelude::*;
use rand::{prelude::SmallRng, SeedableRng};
use riggy::collision::bvh::Bvh;
use riggy::common::geometry::*;
use riggy::physics::*;
use riggy::prelude::Float;
use riggy::prelude::Vector2;
use riggy::prelude::World;
use riggy::prelude::PI;
use std::collections::HashSet;

pub struct ExampleState {
    pub world: World,
    pub world_bounds: AABB,
    pub draw_trans: Vector2,
    pub draw_scale: Float,
    pub draw_weight: Float,
    pub dt: Float,
    pub debug: bool,
    pub draw_body_filter: HashSet<usize>,
    pub draw_joint_filter: HashSet<usize>,
    pub rng: SmallRng,
    pub frame: usize,
}

impl ExampleState {
    pub fn new(world: World) -> Self {
        Self {
            world,
            world_bounds: AABB::empty(),
            draw_trans: Vector2::zero(),
            draw_scale: 1.0,
            draw_weight: 2.0,
            dt: 0.0,
            frame: 0,
            debug: false,
            draw_body_filter: HashSet::new(),
            draw_joint_filter: HashSet::new(),
            rng: SmallRng::seed_from_u64(1337),
        }
    }
}

pub struct ExampleApp {}

impl ExampleApp {
    pub fn run(model: app::ModelFn<ExampleState>) {
        env_logger::init();
        nannou::app(model)
            .update(Self::update)
            .simple_window(Self::view)
            .fullscreen()
            .run()
    }

    fn update(app: &App, state: &mut ExampleState, update: Update) {
        // Nannou is currently missing feature to limit FPS
        // Ugly hack to limit it ourselves
        static FPS_LIMIT: Float = 1.0 / 60.0;

        println!(
            "{} FPS, {} ms",
            app.fps(),
            update.since_last.as_secs_f64()
        );
        state.dt += update.since_last.as_secs_f64() as Float;
        state.frame += 1;

        if state.dt > FPS_LIMIT {
            state.world.update(FPS_LIMIT);
            state.dt -= FPS_LIMIT;
        }

        state.draw_weight = 2.0 / state.draw_scale;
    }

    fn view(app: &App, state: &ExampleState, frame: Frame) {
        Self::render_world(&app, &frame, state);
    }

    fn render_world(app: &App, frame: &Frame, state: &ExampleState) {
        let window = app.window(app.window_id()).unwrap();
        let scale = state.draw_scale * window.inner_size_points().1 / 1240 as Float;
        let draw = app.draw().scale(scale);
        let draw = draw.translate(Vector3::new(state.draw_trans.x, state.draw_trans.y, 0.0));
        draw.background().color(WHITE);

        for joint in state.world.joints() {
            if !state.draw_joint_filter.contains(&joint.id()) {
                Self::render_joint(&draw, joint, &BLACK, state);
            }
        }

        for shape in state.world.collision_shapes() {
            if !state.draw_body_filter.contains(&shape.body) {
                Self::render_shape(&draw, &shape.value, &BLACK, state);
            }
        }

        if state.debug {
            for contact in state.world.contacts() {
                Self::render_contact(&draw, &contact, &RED, state);
            }

            Self::render_bvh(&draw, state.world.bvh(), &LAWNGREEN, state);
        }

        draw.to_frame(app, frame).unwrap();
    }

    fn render_shape(draw: &Draw, shape: &Shape, color: &Srgb<u8>, state: &ExampleState) {
        match shape {
            Shape::Circle(shape) => Self::render_circle(draw, &shape, color, state),
            Shape::Capsule(shape) => Self::render_capsule(draw, &shape, color, state),
            Shape::Polygon(shape) => Self::render_poly(draw, &shape, color, state),
        }
    }

    fn render_circle(draw: &Draw, circle: &Circle, color: &Srgb<u8>, state: &ExampleState) {
        draw.ellipse()
            .stroke(*color)
            .stroke_weight(state.draw_weight)
            .radius((circle.radius) as f32)
            .x_y((circle.origin.x) as f32, (circle.origin.y) as f32);
    }

    fn render_capsule(draw: &Draw, capsule: &Capsule, color: &Srgb<u8>, state: &ExampleState) {
        const POINT_COUNT: usize = 14;
        let line = capsule.line.end - capsule.line.start;
        let angle = -line.y.atan2(line.x);

        let points = (0..POINT_COUNT).map(|i| {
            let (r, point) = if i < POINT_COUNT / 2 {
                (
                    angle + 2.0 * PI * (i as Float / (POINT_COUNT - 1) as Float),
                    capsule.line.end,
                )
            } else {
                (
                    angle + 2.0 * PI * (i as Float / (POINT_COUNT - 1) as Float),
                    capsule.line.start,
                )
            };
            let x = r.sin() * capsule.radius + point.x;
            let y = r.cos() * capsule.radius + point.y;
            nannou::prelude::pt2((x) as f32, (y) as f32)
        });

        draw.polygon()
            .stroke(*color)
            .stroke_weight(state.draw_weight)
            .points(points);
    }

    fn render_poly(draw: &Draw, col: &Polygon, color: &Srgb<u8>, state: &ExampleState) {
        let points = col
            .vertices
            .iter()
            .map(|v| nannou::prelude::pt2((v.x) as f32, (v.y) as f32));
        draw.polygon()
            .stroke(*color)
            .stroke_weight(state.draw_weight)
            .points(points);
    }

    fn render_bvh(draw: &Draw, bvh: &Bvh, color: &Srgb<u8>, state: &ExampleState) {
        for node in &bvh.nodes {
            draw.rect()
                .color(Self::transparent())
                .stroke(*color)
                .stroke_weight(state.draw_weight)
                .w_h((node.aabb.size().x) as f32, (node.aabb.size().y) as f32)
                .x_y((node.aabb.center().x) as f32, (node.aabb.center().y) as f32);
        }
    }

    fn render_joint(draw: &Draw, joint: &DistanceJoint, color: &Srgb<u8>, state: &ExampleState) {
        let body1 = state.world.body(joint.body1());
        let body2 = state.world.body(joint.body2());
        let start = body1.transform.local_to_world(joint.pivot1);
        let end = body2.transform.local_to_world(joint.pivot2);
        draw.line()
            .start(nannou::prelude::pt2(start.x as f32, start.y as f32))
            .end(nannou::prelude::pt2(end.x as f32, end.y as f32))
            .color(*color)
            .weight(state.draw_weight);
    }

    fn render_contact(draw: &Draw, contact: &Contact, color: &Srgb<u8>, state: &ExampleState) {
        for i in 0..contact.contact_count as usize {
            let contact_point = contact.contact_points[i];
            let normal = contact.normal;
            draw.ellipse()
                .color(Self::transparent())
                .stroke(*color)
                .stroke_weight(0.5 * state.draw_weight)
                .radius((2.0) as f32)
                .x_y(contact_point.x as f32, contact_point.y as f32);

            draw.line()
                .color(nannou::prelude::YELLOW)
                .weight(0.5 * state.draw_weight)
                .start(nannou::geom::Vector2 {
                    x: contact_point.x as f32,
                    y: contact_point.y as f32,
                })
                .end(nannou::geom::Vector2 {
                    x: (contact_point.x + normal.x) as f32,
                    y: (contact_point.y + normal.y) as f32,
                });
        }
    }

    fn transparent() -> LinSrgba<Float> {
        nannou::prelude::lin_srgba(0.0, 0.0, 0.0, 0.0)
    }
}
