mod example;

use example::{ExampleApp, ExampleState};
use rand::Rng;
use riggy::prelude::*;

const WORLD_SIZE: Float = 50.0;
const GRAVITY: Float = 9.81;
const FLOOR_THICKNESS: Float = 1.0;

const CIRCLE_COUNT: usize = 500;
const CAPSULE_COUNT: usize = 500;
const BOX_COUNT: usize = 500;
const POLYGON_COUNT: usize = 500;

fn main() {
    ExampleApp::run(build);
}

fn build(_app: &nannou::App) -> ExampleState {
    // Create a new world and arbitrary world bounds to help placing stuff
    let world = World::new(World::down() * GRAVITY);
    let mut state = ExampleState::new(world);
    state.draw_scale = 12.0;
    state.world_bounds = AABB::at_origin(Vector2::new(16.0 / 9.0, 1.0) * WORLD_SIZE);

    // Create a floor for our rigidbodies to interact with
    build_floor(&mut state);

    // Create different shapes of rigidbodies
    for _ in 0..CIRCLE_COUNT {
        let body = build_rigidbody(&mut state);
        let radius = state.rng.gen_range(0.6..=1.2);
        state.world.update_body(body).with_circle(radius);
    }

    for _ in 0..CAPSULE_COUNT {
        let body = build_rigidbody(&mut state);
        let radius = state.rng.gen_range(0.6..=1.0);
        let height = state.rng.gen_range(1.0..=1.4);
        state.world.update_body(body).with_capsule(height, radius);
    }

    for _ in 0..BOX_COUNT {
        let body = build_rigidbody(&mut state);
        let extents = Vector2::new(
            state.rng.gen_range(0.6..=1.2),
            state.rng.gen_range(0.6..=1.2),
        );
        state.world.update_body(body).with_box(extents);
    }

    for _ in 0..POLYGON_COUNT {
        let body = build_rigidbody(&mut state);

        let radius = state.rng.gen_range(1.0..=1.4);
        let vertex_count = state.rng.gen_range(3..=8);
        let vertices = (0..vertex_count).map(|i| {
            let angle = -2.0 * PI * i as Float / vertex_count as Float;
            radius * Vector2::new(angle.sin(), angle.cos())
        });
        state
            .world
            .update_body(body)
            .with_polygon(vertices.collect());
    }

    state
}

fn build_floor(state: &mut ExampleState) {
    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(Vector2::new(
            state.world_bounds.center().x,
            state.world_bounds.min.y,
        ))
        .with_restitution(0.0)
        .with_friction(0.8)
        .with_box(Vector2::new(
            state.world_bounds.size().x * 0.5,
            FLOOR_THICKNESS,
        ));
}

fn build_rigidbody(state: &mut ExampleState) -> usize {
    state
        .world
        .create_body()
        .with_position(Vector2::new(
            state
                .rng
                .gen_range(state.world_bounds.min.x..=state.world_bounds.max.x),
            state
                .rng
                .gen_range(state.world_bounds.min.y..=state.world_bounds.max.y),
        ))
        .with_rotation(state.rng.gen_range(0.0..2.0 * PI))
        .with_restitution(state.rng.gen_range(0.0..0.5))
        .with_friction(state.rng.gen_range(0.1..1.0))
        .with_mass(100.0)
        .with_inertia(100.0)
        .finish()
}
