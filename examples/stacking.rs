mod example;

use example::{ExampleApp, ExampleState};
use lerp::Lerp;
use riggy::prelude::*;

const WORLD_SIZE: Float = 20.0;
const GRAVITY: Float = 9.81;

const FLOOR_THICKNESS: Float = 0.3;
const STACK_COUNT: usize = 10;
const BOXES_PER_STACK: usize = 5;
const BOX_WIDTH: Float = 1.2;
const BOX_HEIGHT: Float = 1.0;

fn main() {
    ExampleApp::run(build);
}

fn build(_app: &nannou::App) -> ExampleState {
    // Create a new world and arbitrary world bounds to help placing stuff
    let world = World::new(World::down() * GRAVITY);
    let mut state = ExampleState::new(world);
    state.draw_scale = 24.0;
    state.world_bounds = AABB::at_origin(Vector2::new(16.0 / 9.0, 1.0) * WORLD_SIZE);

    // Create floor for our boxes to interact with
    build_floor(&mut state);

    // Create several stacks of boxes
    let starting_height = state.world_bounds.min.y + FLOOR_THICKNESS + BOX_HEIGHT * 0.5;
    for i in 0..STACK_COUNT {
        for j in 0..BOXES_PER_STACK {
            let t = (i + 1) as Float / (STACK_COUNT + 1) as Float;
            let stack_pos = state.world_bounds.min.x.lerp(state.world_bounds.max.x, t);
            let box_pos = starting_height + j as Float * (BOX_HEIGHT);
            let position = Vector2::new(stack_pos, box_pos);
            build_box(&mut state, position);
        }
    }

    state
}

fn build_floor(state: &mut ExampleState) {
    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(Vector2::new(
            state.world_bounds.center().x,
            state.world_bounds.min.y,
        ))
        .with_restitution(0.2)
        .with_friction(0.5)
        .with_box(Vector2::new(
            state.world_bounds.size().x * 0.5,
            FLOOR_THICKNESS,
        ));
}

fn build_box(state: &mut ExampleState, position: Vector2) {
    state
        .world
        .create_body()
        .with_position(position)
        .with_restitution(0.1)
        .with_friction(0.25)
        .with_mass(100.0)
        .with_inertia(100.0)
        .with_box(Vector2::new(0.5 * BOX_WIDTH, 0.5 * BOX_HEIGHT));
}
