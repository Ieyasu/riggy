mod example;

use example::{ExampleApp, ExampleState};
use lerp::Lerp;
use riggy::prelude::*;

const WORLD_SIZE: Float = 20.0;
const GRAVITY: Float = 9.81;

const FLOOR_THICKNESS: Float = 0.3;
const FLOOR_COUNT: usize = 8;
const BOXES_PER_FLOOR: usize = 5;
const BOX_WIDTH: Float = 1.2;
const BOX_HEIGHT: Float = 1.0;

fn main() {
    ExampleApp::run(build);
}

fn build(_app: &nannou::App) -> ExampleState {
    // Create a new world and arbitrary world bounds to help placing stuff
    let world = World::new(World::down() * GRAVITY);
    let mut state = ExampleState::new(world);
    state.draw_scale = 24.0;
    state.world_bounds = AABB::at_origin(Vector2::new(16.0 / 9.0, 1.0) * WORLD_SIZE);

    // Create floors and boxes with different frictions
    for i in 0..FLOOR_COUNT {
        // Create floors with different frictions for our boxes to interact with
        let t = i as Float / (FLOOR_COUNT - 1) as Float;

        let friction = t;
        let floor_pos = Vector2::new(
            state.world_bounds.center().x,
            state
                .world_bounds
                .min
                .y
                .lerp(state.world_bounds.max.y * 0.8, t),
        );
        build_floor(&mut state, floor_pos, friction);

        for j in 0..BOXES_PER_FLOOR {
            // Create boxes with different frictions on each floor
            let t = (j + 1) as Float / (BOXES_PER_FLOOR + 1) as Float;

            let friction = j as Float / (BOXES_PER_FLOOR - 1) as Float;
            let position = Vector2::new(
                state.world_bounds.min.x.lerp(state.world_bounds.max.x, t),
                floor_pos.y + FLOOR_THICKNESS + 0.5 * BOX_HEIGHT,
            );
            build_box(&mut state, position, friction);
        }
    }

    state
}

fn build_floor(state: &mut ExampleState, position: Vector2, friction: Float) {
    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(position)
        .with_restitution(0.0)
        .with_friction(friction)
        .with_box(Vector2::new(
            state.world_bounds.size().x * 0.5,
            FLOOR_THICKNESS,
        ));
}

fn build_box(state: &mut ExampleState, position: Vector2, friction: Float) {
    state
        .world
        .create_body()
        .with_position(position)
        .with_velocity(Vector2::new(8.0, 0.0))
        .with_restitution(0.3)
        .with_friction(friction)
        .with_mass(20.0)
        .with_inertia(20.0)
        .with_box(Vector2::new(0.5 * BOX_WIDTH, 0.5 * BOX_HEIGHT));
}
