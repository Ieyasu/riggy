mod example;

use example::{ExampleApp, ExampleState};
use lerp::Lerp;
use riggy::prelude::*;

const WORLD_SIZE: Float = 20.0;
const GRAVITY: Float = 9.81;

const FLOOR_THICKNESS: Float = 0.4;
const BALL_COUNT: usize = 12;
const BALL_RADIUS: Float = 1.0;

fn main() {
    ExampleApp::run(build);
}

fn build(_app: &nannou::App) -> ExampleState {
    // Create a new world and arbitrary world bounds to help placing stuff
    let world = World::new(World::down() * GRAVITY);
    let mut state = ExampleState::new(world);
    state.draw_scale = 24.0;
    state.world_bounds = AABB::at_origin(Vector2::new(16.0 / 9.0, 1.0) * WORLD_SIZE);

    // Create floor for our balls to interact with
    build_floor(&mut state);

    for j in 0..BALL_COUNT {
        // Create balls with different restitutions (bounciness)
        let t = (j + 1) as Float / (BALL_COUNT + 1) as Float;

        let restitution = (j as Float / (BALL_COUNT - 1) as Float - 0.01).sqrt();
        let position = Vector2::new(
            state.world_bounds.min.x.lerp(state.world_bounds.max.x, t),
            10.0,
        );
        build_ball(&mut state, position, restitution);
    }

    state
}

fn build_floor(state: &mut ExampleState) {
    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(Vector2::new(
            state.world_bounds.center().x,
            state.world_bounds.min.y,
        ))
        .with_restitution(0.0)
        .with_friction(0.5)
        .with_box(Vector2::new(
            state.world_bounds.size().x * 0.5,
            FLOOR_THICKNESS,
        ));
}

fn build_ball(state: &mut ExampleState, position: Vector2, restitution: Float) {
    state
        .world
        .create_body()
        .with_position(position)
        .with_restitution(restitution)
        .with_friction(0.5)
        .with_mass(20.0)
        .with_inertia(20.0)
        .with_circle(BALL_RADIUS);
}
