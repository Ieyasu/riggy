mod example;

use example::{ExampleApp, ExampleState};
use rand::Rng;
use riggy::prelude::*;

const WORLD_SIZE: Float = 100.0;
const GRAVITY: Float = 9.81;
const FLOOR_THICKNESS: Float = 1.5;

const SOFTBODY_COUNT: usize = 10;
const CIRCLE_COUNT: usize = 500;
const CAPSULE_COUNT: usize = 500;
const BOX_COUNT: usize = 500;
const POLYGON_COUNT: usize = 500;

fn main() {
    ExampleApp::run(build);
}

fn build(_app: &nannou::App) -> ExampleState {
    // Create a new world and arbitrary world bounds to help placing stuff
    let world = World::new(World::down() * GRAVITY);
    let mut state = ExampleState::new(world);
    state.draw_scale = 6.0;
    state.draw_trans = Vector2::new(0.0, 120.0);
    state.world_bounds = AABB::at_origin(Vector2::new(16.0 / 9.0, 1.0) * WORLD_SIZE);

    // Create a pair of floors for our bodies to interact with
    build_floors(&mut state);

    // Create different shapes of rigidbodies
    for _ in 0..CIRCLE_COUNT {
        let body = build_rigidbody(&mut state);
        let radius = state.rng.gen_range(0.6..=1.2);
        state.world.update_body(body).with_circle(radius);
    }

    for _ in 0..CAPSULE_COUNT {
        let body = build_rigidbody(&mut state);
        let radius = state.rng.gen_range(0.6..=1.0);
        let height = state.rng.gen_range(1.0..=1.4);
        state.world.update_body(body).with_capsule(height, radius);
    }

    for _ in 0..BOX_COUNT {
        let body = build_rigidbody(&mut state);
        let extents = Vector2::new(
            state.rng.gen_range(0.6..=1.2),
            state.rng.gen_range(0.6..=1.2),
        );
        state.world.update_body(body).with_box(extents);
    }

    for _ in 0..POLYGON_COUNT {
        let body = build_rigidbody(&mut state);

        let radius = state.rng.gen_range(1.0..=1.4);
        let vertex_count = state.rng.gen_range(3..=8);
        let vertices = (0..vertex_count).map(|i| {
            let angle = -2.0 * PI * i as Float / vertex_count as Float;
            radius * Vector2::new(angle.sin(), angle.cos())
        });
        state
            .world
            .update_body(body)
            .with_polygon(vertices.collect());
    }

    // Create softbodies
    for _ in 0..SOFTBODY_COUNT {
        build_softbody(&mut state);
    }

    state
}

fn build_floors(state: &mut ExampleState) {
    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(Vector2::new(
            state.world_bounds.center().x,
            state.world_bounds.min.y - 90.0,
        ))
        .with_restitution(0.4)
        .with_friction(0.8)
        .with_rotation(0.1 * PI)
        .with_box(Vector2::new(
            state.world_bounds.size().x * 0.5,
            FLOOR_THICKNESS,
        ));

    state
        .world
        .create_body()
        .with_is_static(true)
        .with_position(Vector2::new(
            state.world_bounds.min.x,
            state.world_bounds.min.y,
        ))
        .with_restitution(0.4)
        .with_friction(0.8)
        .with_rotation(-0.1 * PI)
        .with_box(Vector2::new(
            state.world_bounds.size().x * 0.5,
            FLOOR_THICKNESS,
        ));
}

fn build_rigidbody(state: &mut ExampleState) -> usize {
    state
        .world
        .create_body()
        .with_position(Vector2::new(
            state
                .rng
                .gen_range(state.world_bounds.min.x..=state.world_bounds.max.x),
            state
                .rng
                .gen_range(state.world_bounds.min.y..=state.world_bounds.max.y),
        ))
        .with_rotation(state.rng.gen_range(0.0..2.0 * PI))
        .with_restitution(state.rng.gen_range(0.0..0.5))
        .with_friction(state.rng.gen_range(0.1..1.0))
        .with_mass(100.0)
        .with_inertia(100.0)
        .finish()
}

fn build_softbody(state: &mut ExampleState) {
    let radius = state.rng.gen_range(4.0..=12.0);
    let thickness = radius * 0.2;
    let vert_radius = thickness / 3.0;
    let edge_count = state.rng.gen_range(24..=32);
    let position = Vector2::new(
        state
            .rng
            .gen_range(state.world_bounds.min.x..=state.world_bounds.max.x),
        state
            .rng
            .gen_range(state.world_bounds.min.y..=state.world_bounds.max.y),
    );

    let pos_outer = position + Vector2::new(radius, 0.0);
    let vert_outer_first = build_vertex(state, pos_outer, vert_radius);

    let pos_inner = position + Vector2::new(radius - thickness, 0.0);
    let vert_inner_first = build_vertex(state, pos_inner, vert_radius);

    let mut vert_outer_prev = vert_outer_first;
    let mut vert_inner_prev = vert_inner_first;
    for i in 1..edge_count {
        let angle = 2.0 * PI * i as Float / edge_count as Float;
        let offset = Vector2::new(angle.cos(), angle.sin());

        let pos_outer = position + offset * radius;
        let vert_outer = build_vertex(state, pos_outer, vert_radius);

        let pos_inner = position + offset * (radius - thickness);
        let vert_inner = build_vertex(state, pos_inner, vert_radius);

        build_spring(state, vert_inner, vert_outer, false);
        build_spring(state, vert_inner, vert_inner_prev, true);
        build_spring(state, vert_outer, vert_outer_prev, true);
        build_spring(state, vert_outer, vert_inner_prev, false);
        build_spring(state, vert_inner, vert_outer_prev, false);

        vert_inner_prev = vert_inner;
        vert_outer_prev = vert_outer;
    }

    build_spring(state, vert_inner_first, vert_outer_first, false);
    build_spring(state, vert_inner_first, vert_inner_prev, true);
    build_spring(state, vert_outer_first, vert_outer_prev, true);
    build_spring(state, vert_outer_first, vert_inner_prev, false);
    build_spring(state, vert_inner_first, vert_outer_prev, false);
}

fn build_vertex(state: &mut ExampleState, position: Vector2, radius: Float) -> usize {
    state
        .world
        .create_body()
        .with_position(position)
        .with_restitution(0.5)
        .with_friction(1.0)
        .with_mass(100.0)
        .with_circle(radius)
        .finish()
}

fn build_spring(state: &mut ExampleState, body1: usize, body2: usize, draw: bool) {
    let joint = state
        .world
        .create_distance_joint(body1, body2)
        .with_frequency(1000.0)
        .with_damping(0.5)
        .finish();
    if !draw {
        state.draw_joint_filter.insert(joint);
    }
}
