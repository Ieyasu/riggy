# Examples

This directory contains example scenes showing different features of the engine.

## friction

Demonstrates the effect of friction on sliding objects.

## ramps

Demonstrates the interactions between large number of rigidbodies and softbodies.

## restitution

Demonstrates the effect of restitution (bounciness) on bouncing objects.

## riggy

Welcome to riggy!

## rigidbody

Demonstrates the interactions between large number of rigidbodies.

## softbody

Demonstrates how softbodies can be built from a network of springs and small rigidbodies.

## stacking

Demonstrates stacking boxes. Currently unstable.
